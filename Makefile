# # MakeFile

# - Directory structure:
# 
# |-build
# 	|-lib*.a			<- Static Library file
# 	|-obj				<- Library object files
#	|-proj
# 	  |-proj_0
#       |-obj
# 		  |-*.o			<- Project object files
# 		|-bin			<- Project binaries
#		  |-debug.out
#         |-release.out
#     |-proj_1
#     ...
# |-doc					<- Library documentation
# |-include				<- Library include path
#   |-*.hpp				<- Library header files
#   |-*.tpp				<- Library template definiton files
# |-proj
#   |-proj_0
#     |-*.cpp			<- Project source files
#     |-*.hpp			<- Project headers
#   |-proj_1
#   ...
# |-src
#   |-*.cpp				<- Library source files

# # Files

# - Directory structure
REPO_ROOT_DIR = $(shell pwd)
INCLUDE_PATH = ./include
SOURCE_PATH = ./src
BUILD_PATH = ./build
PROJECTS_PATH = ./proj

# - Library
PCH_H = $(INCLUDE_PATH)/pch.hpp
PCH_OUT = $(PCH_H).gch

LIB_NAME = tsp-sa
LIB_INCS = -I $(INCLUDE_PATH)
LIB_HEADER_FILES = $(shell find $(INCLUDE_PATH) -name '*.hpp') $(shell find $(INCLUDE_PATH) -name '*.tpp')
LIB_SRC_FILES = $(shell find $(SOURCE_PATH) -name '*.cpp')
LIB_OBJ_PATH = $(BUILD_PATH)/obj
LIB_OBJ_FILES = $(LIB_SRC_FILES:$(SOURCE_PATH)/%.cpp=$(LIB_OBJ_PATH)/%.o)

LIB_SHARED_PATH = $(BUILD_PATH)
LIB_SHARED = $(LIB_STATIC_PATH)/lib$(LIB_NAME).so

LIB_STATIC_PATH = $(BUILD_PATH)
LIB_STATIC = $(LIB_STATIC_PATH)/lib$(LIB_NAME).a

LIB_DIRECTIVES = -DREPO_ROOT_DIR="\"$(REPO_ROOT_DIR)\"" -DTIMING -DPROFILING

# - Project
PROJ = final_costs#test
PROJ_PATH = $(PROJECTS_PATH)/$(PROJ)
PROJ_INCS = $(LIB_INCS) -I $(PROJ_PATH)
PROJ_SRC_FILES := $(shell find $(PROJ_PATH) -name '*.cpp')
PROJ_OBJ_PATH = $(BUILD_PATH)/proj/$(PROJ)/obj
PROJ_OBJ_FILES = $(PROJ_SRC_FILES:$(PROJ_PATH)/%.cpp=$(PROJ_OBJ_PATH)/%.o)

BIN_NAME = $(shell echo $(PROJ) | sed -e 's/\//_/g').out
BIN_PATH = $(shell echo $(BUILD_PATH)/$(PROJ_PATH)/bin | sed -e 's/\/\.//g')

PROJ_DIRECTIVES = $(LIB_DIRECTIVES)

# # Compiler
CC = g++
CXX_STD = -std=gnu++17
CC_WARNS = -pedantic -Wall -Wextra -Wfatal-errors
CC_OPTIMIZATION = -O3 -msse2 #-faggressive-loop-optimizations
CC_DEBUG = -v -Og -g -DDEBUG -fstandalone-debug

# - Flags
CC_PCH_FLAGS = $(CXX_STD) $(CC_OPTIMIZATION) $(CC_WARNS) -x c++-header # -Wno-pragma-once-outside-header
CC_LIB_FLAGS = $(CXX_STD) $(CC_OPTIMIZATION) $(CC_WARNS) $(LIB_INCS) $(LIB_DIRECTIVES) -fPIC
CC_RELEASE_FLAGS = $(CXX_STD) $(CC_OPTIMIZATION) $(CC_WARNS) $(PROJ_INCS) $(PROJ_DIRECTIVES)
CC_DEBUG_FLAGS = $(CXX_STD) $(CC_DEBUG) $(CC_WARNS) $(PROJ_DIRECTIVES) $(PROJ_INCS)

# # Linker
LD = $(CC)
LD_FLAGS = $(CXX_STD) $(CC_OPTIMIZATION) $(CC_WARNS) $(LIB_INCS) -static
LD_LIB = -L$(BUILD_PATH) -l$(LIB_NAME)

# # Debugger
DB = lldb
DB_FLAGS = # --arch x86_64

# # Documentation
DOC_GEN = doxygen
DOC_PATH = ./doc
DOC_CONFIG = ./dconfig

.PHONY: all check_proj lib_static lib_shared build run clean_pch clean_lib clean_proj clean_bins clean_docs clean docs
all: lib_static lib_shared build

# - Compilation

# Precompiled header
$(PCH_OUT): $(PCH_H)
	@printf "Pre-compiling header...\n"
	@$(CC) $(CC_PCH_FLAGS) $< -o $@

# Library
$(LIB_OBJ_PATH)/%.o: $(SOURCE_PATH)/%.cpp $(PCH_OUT) $(LIB_HEADER_FILES)
	@mkdir -p $(@D)
	@printf "Compiling $(notdir $<) (library)\n"
	@$(CC) $(CC_LIB_FLAGS) -c $< -o $@

# Project
$(PROJ_OBJ_PATH)/%.o: $(PROJ_PATH)/%.cpp $(PCH_OUT) $(LIB_HEADER_FILES)
	@mkdir -p $(@D)
	@printf "Compiling $(notdir $<) (project)\n"
	@$(CC) $(CC_RELEASE_FLAGS) -c $< -o $@

# - Static Library

$(LIB_STATIC): $(LIB_OBJ_FILES)
	@mkdir -p $(BUILD_PATH)
	@printf "Creating static library $(LIB_NAME)\n"
	@ar rcs $(LIB_STATIC) $(LIB_OBJ_FILES)

# - Dynamic Library

$(LIB_SHARED): $(LIB_OBJ_FILES)
	@mkdir -p $(BUILD_PATH)
	@printf "Creating shared library $(LIB_NAME)\n"
	@$(CC) $(CC_LIB_FLAGS) -shared -o $(LIB_SHARED) $(LIB_OBJ_FILES)

# - Linking

$(BIN_PATH)/$(BIN_NAME): $(LIB_STATIC) $(PROJ_OBJ_FILES)
	@printf "Linking...\n\n"
	@$(LD) $(LD_FLAGS) $(PROJ_OBJ_FILES) $(LD_LIB) -o $(BIN_PATH)/$(BIN_NAME)

# - Build Library

lib_static: $(LIB_STATIC)

lib_shared: $(LIB_SHARED)

# - Check if project exist

check_proj:
	@printf "Checking project...\n"
	@if test ! -d "$(PROJ_PATH)"; then \
		printf "PROJECT NOT FOUND\n"; \
		exit 1; \
    fi
	@mkdir -p $(PROJ_OBJ_PATH) $(BIN_PATH)

check_args_script:
	@printf "Checking args script...\n"
	@if test ! -f "$(PROJ_ARGS_SCRIPT)"; then \
		printf "ARGS SCRIPT NOT FOUND\n"; \
		exit 1; \
    fi

# - Linking

build: check_proj $(BIN_PATH)/$(BIN_NAME)

# - Running

run: build
	@printf "Running binary...\n\n"
	@cd $(PROJ_PATH); time $(REPO_ROOT_DIR)/$(BIN_PATH)/$(BIN_NAME)

# - Debugging

build_debug_proj: check_proj
	$(CC) $(CC_DEBUG_FLAGS) $(LIB_SRC_FILES) $(PROJ_SRC_FILES) -o $(BIN_PATH)

debug_proj: proj_debug_build
	$(DB) $(DB_FLAGS) $(BIN_PATH)/$(BIN_NAME)

# - Documentation

docs: clean_docs
	@printf "Generating documentation...\n"
	@mdir -p $(DOC_PATH)
	@$(DOC_GEN) $(DOC_CONFIG)

# - Cleaning

# Remove pch
clean_pch:
	@printf "Cleaning precompiled header...\n"
	@rm -f $(PCH_OUT)

# Remove library object files
clean_lib:
	@printf "Cleaning library files...\n"
	@rm -rf $(LIB_OBJ_PATH)
	@rm -rf $(LIB_STATIC)
	@rm -rf $(LIB_SHARED)

# Remove project object files and target binary
clean_proj:
	@printf "Cleaning project files...\n"
	@rm -rf $(PROJ_OBJ_PATH)
	@rm -f $(BIN_PATH)/$(BIN_NAME)

clean_bins:
	@printf "Deleting binaries...\n"
	@rm -f $(dir $(BIN_PATH))/*

clean_docs:
	@printf "Cleaning documentation...\n"
	@rm -rf $(DOC_PATH)/*

clean: clean_pch clean_lib check_proj clean_proj
	@printf "All clear\n\n"