#pragma once

/**
 * \file core.hpp
 * \author eliseuv (eliseuv816@gmail.com)
 * \brief Macros and functions used by the library
 * 
 */

#include "pch.hpp"

// # Metaprogramming

/// Test if a parameter pack Ts... is of the given type Target
template <typename Target, typename... Ts>
inline constexpr bool areSameType = std::conjunction_v<std::is_same<Target, Ts>...>;

/**
 * \brief Static array populated by values given by variadic template.
 * 
 * \tparam T: Type of values given
 * \tparam N: Number of values given
 * \tparam Ts: Template parameter pack of values
 * \param values: Values to populate static array
 * \return constexpr std::array<T, N>: 
 */
template <typename T, size_t N, typename... Ts>
inline constexpr std::array<T, N> variadicTemplateToArray(Ts&&... values) {
    static_assert(N == sizeof...(values));
    static_assert(areSameType<T, Ts...>);
    return std::array<T, N> { std::forward<Ts>(values)... };
}

// # String manipulation

/// Stringify
#define XSTRING(x) #x
/// Stringify
#define STRING(x) XSTRING(x)

#define CAT_(a, b) a ## b
#define CAT(a, b) CAT_(a, b)
/// Make a name unique by adding the line number to it
#define UNIQUE(varname) CAT(varname, __LINE__)

// - Concatenate strings and variables

/**
 * \brief Concatenate values in a string.
 * Final call
 */
template <typename T>
void concatString(std::stringstream& ss, T final_value) {
    ss << final_value;
}

/**
 * \brief Concatenate values in a string.
 * Recursive call
 */
template <typename T, typename... Ts>
void concatString(std::stringstream& ss, T value, Ts... other_values) {
    ss << value;
    concatString(ss, other_values...);
}

/**
 * \brief Concatenate values in a string
 * Base call: calls recursive function on values
 * 
 * \tparam Ts: Types of values
 * \param values: Values to be concatenated
 * \return std::string: Concatenated string with values
 */
template <typename... Ts>
std::string concatString(Ts... values) {
    std::stringstream ss;
    concatString(ss, values...);
    return ss.str();
}

// - Print variable name and value

/**
 * \brief Returns string containig variable name and its value.\n
 *  `int foo = 42;`\n
 *  `STR_VAR(foo) = "foo=42"`
 */
#define STR_VAR(VAR) concatString(STRING(VAR), '=', (VAR))

// - String parsing

/**
 * \brief Parse string into variable
 * 
 * \tparam T: Type of value to parsed
 * \param str: String to be parsed
 * \return T: Parsed value
 */
template <typename T>
inline T parseString(const char* str) {
    std::istringstream ss(str);
    T result;
    ss >> result;
    return result;
}

// # Default program

/// Default main function that accepts arguments
#define MAIN_FUNCTION int main(int argc, char const** argv)

// - Parse Program Arguments

/**
 * \brief Parse arguments given to binary into variables in order.
 * Arguments of the main function are expected to be (int argc, char const** argv)
 */
#define PARSE_ARGS(...) parseArgs(argc, argv, __VA_ARGS__);

/**
 * \brief Parse Program Arguments.
 * Final call: parse i-th argument to var
 */
template <typename T>
inline void parseArgs(int i, int argc, char const** argv, T& final_var) {
    if (i < argc) {
        final_var = parseString<T>(argv[i]);
        std::cout << "arg" << i << "=" << final_var << std::endl;
    }    
}

/**
 * \brief Parse Program Arguments.
 * Recursive call
 */
template <typename T, typename... Ts>
inline void parseArgs(int i, int argc, char const** argv, T& var, Ts&... other_vars) {
    if (i < argc) {
        var = parseString<T>(argv[i]);
        std::cout << "arg" << i << "=" << var << std::endl;
        parseArgs(i+1, argc, argv, other_vars...);
    }       
}

/**
 * \brief Parse Program Arguments
 * Base call: calls recursive function on arguments
 * 
 * \tparam Ts: Types of the other arguments
 * \param argc: Argument count
 * \param argv: Argument values
 * \param vars: Variables that will store the parsed arguments
 */
template <typename... Ts>
inline void parseArgs(int argc, char const** argv, Ts&... vars) {
    parseArgs(1, argc, argv, vars...);
}

// # Debugging

#ifdef DEBUG

/// Assert condition
#define ASSERT(condition)   if (!(condition)) {                                             \
                                printf("ERROR at %s in %s line %d: Assertion %s failed\n",  \
                                __PRETTY_FUNCTION__, __FILE__, __LINE__, #condition);              \
                                exit(EXIT_FAILURE);                                         \
                            }

#else

/// Ignore assert macro
#define ASSERT(condition)   (void(0));

#endif