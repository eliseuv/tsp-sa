/**
 * \file pch.hpp
 * \author eliseuv (eliseuv816@gmail.com)
 * \brief Header to be precompiled
 * 
 */

#include <cstdlib>
#include <typeinfo>
#include <type_traits>
#include <memory>
#include <iterator>
#include <functional>
#include <utility>
#include <algorithm>
#include <chrono>
#include <thread>

// - Data Structures
#include <stack>
#include <string>
#include <array>
#include <initializer_list>
#include <vector>
#include <tuple>
#include <set>
#include <map>

// - Numerical
#include <cmath>
#include <numeric>
#include <limits>
#include <complex>
#include <random>

// - IO
#include <cstdio>
#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>

// - Eigen (Linear Algebra)
#define EIGEN_STACK_ALLOCATION_LIMIT 0
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Dense>