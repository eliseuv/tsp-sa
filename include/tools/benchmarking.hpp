#pragma once

/**
 * \file benchmarking.hpp
 * \author eliseuv (eliseuv816@gmail.com)
 * \brief Benchmarking utilities.
 * 
 */

#include "core.hpp"

// # Simple Timer

#define TIMING 1

#if TIMING
/**
 * \brief Time a function call.
 * Will print time after the call to stantdard output.
 * \code
 *  TIME_CALL(call();)
 * \endcode
 */
#define TIME_CALL(call) global_simple_timer.start();    \
                        call                            \
                        std::cout << __LINE__ << ". " << #call << " : " << global_simple_timer.read() << 's' << std::endl;
#else
#define TIME_CALL(call) call
#endif

/// Simple timer that starts timing when constructed
class SimpleTimer
{
public:

    using Clock = std::chrono::high_resolution_clock;

private:

    std::chrono::time_point<Clock> start_timepoint_;

public:

    inline SimpleTimer()
    : start_timepoint_(Clock::now())
    {}

    inline void start()
    { start_timepoint_ = Clock::now(); }

    inline double read() const
    {
        std::atomic_thread_fence(std::memory_order_relaxed);
        auto counted_time = std::chrono::duration_cast<std::chrono::nanoseconds>(Clock::now() - start_timepoint_).count();
        std::atomic_thread_fence(std::memory_order_relaxed);
        double elapsed_secs = counted_time*1e-9;
        return elapsed_secs;
    }

} global_simple_timer; // SimpleTimer

// # Scope Timer

#if TIMING
/**
 * \brief Time a scope.
 * \code
 *  {
 *      TIME_SCOPE("Example scope")
 *      func1();
 *      func2();
 *  }
 * \endcode
 */
#define TIME_SCOPE(scope_name) ScopeTimer scope_timer ## __LINE__(__LINE__, scope_name);
#else
#define TIME_SCOPE(scope_name)
#endif

/// RAII Timer with a name. Starts timing when constructed and stops when destroyed at the end of the scope.
class ScopeTimer: public SimpleTimer
{
private:

    int line_;
    const char* scope_name_;

public:

    inline ScopeTimer(int line, const char* scope_name)
    : SimpleTimer(),
      line_(line), scope_name_(scope_name)
    {}

    inline ~ScopeTimer()
    { print(); }

    inline void print() const
    { std::cout << line_ << ". " << scope_name_ << " : " << read() << "s" << std::endl; }

}; // ScopeTimer

/**
 * \brief Fully featured timer that starts counting when it is constructed
 * 
 * \tparam Clock=std::chrono::high_resolution_clock Which clock to use
 */
template <typename Clock=std::chrono::high_resolution_clock>
class Timer
{
private:

    const char* name_;
    const typename Clock::time_point start_timepoint_;

public:

    /// Constructor
    inline Timer(const char* name="Timer")
    : name_(name),
      start_timepoint_(Clock::now())
    {}

    /// Get elapsed time
    template <typename Rep=typename Clock::duration::rep, typename Units=typename Clock::duration>
    inline Rep read() const
    {
        std::atomic_thread_fence(std::memory_order_relaxed);
        auto counted_time = std::chrono::duration_cast<Units>(Clock::now() - start_timepoint_).count();
        std::atomic_thread_fence(std::memory_order_relaxed);
        return static_cast<Rep>(counted_time);
    }

    /// Time reading in seconds
    template <typename Rep=double>
    inline Rep readSec() const
    {
        std::atomic_thread_fence(std::memory_order_relaxed);
        auto counted_time = std::chrono::duration_cast<std::chrono::nanoseconds>(Clock::now() - start_timepoint_).count();
        std::atomic_thread_fence(std::memory_order_relaxed);
        auto read_ns = static_cast<unsigned long long>(counted_time);
        return (read_ns*1e-9);
    }

    /// Print time elapsed along with a short description
    inline void print(std::ostream& output_stream) const
    {
        double seconds = readSec<double>();
        output_stream << name_ << " : " << seconds << "s" << std::endl;
    }

}; // Timer

// - Timer instatiations

using PreciseTimer   = Timer<>;
using SystemTimer    = Timer<std::chrono::system_clock>;
using MonotonicTimer = Timer<std::chrono::steady_clock>;

// # Profiling

#define PROFILING 1

#if PROFILING
    #define PROFILE_SCOPE(name) Profiler::Timer timer##__LINE__(name);
    #define PROFILE_FUNCTION TIME_SCOPE(__PRETTY_FUNCTION__)
    #define PROFILE_CALL(call) Profiler::Timer timer(#call); call timer.~Timer();
#else
    #define PROFILE_SCOPE(name)
    #define PROFILE_FUNCTION (void(0));
    #define PROFILE_CALL(call)
#endif

/**
 * \brief 
 * 
 */
class Profiler
{
public:

    /**
     * \brief 
     * 
     */
    class Timer
    {
    public:
        using Clock = std::chrono::high_resolution_clock;
    private:
        const char* name_;
        std::chrono::time_point<Clock> start_timepoint_;
    public:
        inline Timer(const char* name)
        : name_(name)
        { start_timepoint_ = Clock::now(); }
        inline ~Timer()
        {
            std::chrono::time_point<Clock> end_timepoint = Clock::now();
            long long start = std::chrono::time_point_cast<std::chrono::microseconds>(start_timepoint_).time_since_epoch().count();
            long long end = std::chrono::time_point_cast<std::chrono::microseconds>(end_timepoint).time_since_epoch().count();
            uint32_t thread_id = std::hash<std::thread::id>{}(std::this_thread::get_id());

            Profiler::instance().writeResult({ name_, start, end, thread_id });
        }
    }; // Profiler::Timer

private:

    struct Result {
        std::string name;
        long long start, end;
        uint32_t thread_id;
    }; // Profiler::Result

private:

    std::ofstream output_stream_;
    size_t profile_count_;

    /// Default constructor
    Profiler()
    : profile_count_(0)
    {}

public:

    /// Get singleton instance
    inline static Profiler& instance()
    {
        static Profiler instance;
        return instance;
    }

    inline void beginSession(const std::string& filepath="profiling.json")
    {
        output_stream_.open(filepath, std::ios::out);
        output_stream_ << "{\"otherData\": {},\"traceEvents\":[" << std::endl;
    }

    inline void endSession()
    {
        output_stream_ << "]}" << std::endl;
        output_stream_.close();
        profile_count_ = 0;
    }

    inline void writeResult(const Result& result)
    {
        if (profile_count_++ > 0)
            output_stream_ << ",";

        std::string name = result.name;
        std::replace(name.begin(), name.end(), '"', '\'');

        output_stream_ << "{";
        output_stream_ << "\"cat\":\"function\",";
        output_stream_ << "\"dur\":" << (result.end - result.start) << ',';
        output_stream_ << "\"name\":\"" << name << "\",";
        output_stream_ << "\"ph\":\"X\",";
        output_stream_ << "\"pid\":0,";
        output_stream_ << "\"tid\":" << result.thread_id << ",";
        output_stream_ << "\"ts\":" << result.start;
        output_stream_ << "}";

        output_stream_.flush();
    }

}; // Profiler