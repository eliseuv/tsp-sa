#pragma once

/**
 * \file gnuplot.hpp
 * \author eliseuv (eliseuv816@gmail.com)
 * \brief Integration with Gnuplot.
 *
 */

#include "core.hpp"

// # Gnuplot Interface

/// Gnuplot interface
class Gnuplot {
private:
  FILE *pipe_;

public:
  // - Constructors

  /// Default constructor: opens interface
  inline Gnuplot(void) : pipe_(popen("gnuplot --persist", "w")) {
    if (!pipe_)
      std::cerr << "Gnuplot not found!" << std::endl;
  }

  /// Open interface and give a command
  inline Gnuplot(const std::string &command) : Gnuplot() {
    this->operator()(command);
  }

  // - Destructor

  inline ~Gnuplot() {
    fprintf(pipe_, "exit\n");
    pclose(pipe_);
  }

  // - Enter commands

  /// Directly enter command
  inline void enterCommand(const std::string &command) {
#ifdef DEBUG
    std::cout << "gnuplot: " << command << std::endl;
#endif
    fprintf(pipe_, "%s\n", command.c_str());
    fflush(pipe_);
  }

  /// Directly enter command
  inline void operator()(const std::string &command) { enterCommand(command); }

  /// Directly enter command
  template <typename... Ts> inline void operator()(Ts... values) {
    enterCommand(concatString(values...));
  }

  // - Enter values

  /**
   * \brief Open interface to accept series of points to be plotted
   *
   * \param n_series: Number of series of points
   * \param opts: Options common to all series
   */
  inline void beginPoints(const std::string &opts = "");
  inline void beginPoints(size_t n_series, const std::string &opts = "");

  /**
   * \brief Open interface to accept series of points to be plotted
   *
   * \param n_series: Number of series of points
   * \param opts: Options common to each series series
   */
  inline void beginPoints(size_t n_series,
                          const std::initializer_list<std::string> &opts);

  /// Finish a series of points
  inline void endPoints();

  /// Enter 2D point
  template <typename Real0, typename Real1>
  inline void point2D(const Real0 &v0, const Real1 &v1);

  /// Enter 3D point
  template <typename Real0, typename Real1, typename Real2>
  inline void point3D(const Real0 &v0, const Real1 &v1, const Real2 &v2);

  /**
   * \brief Plot data directly
   *
   * \tparam Real: Real number
   * \param x: x values
   * \param y: y values
   * \param opts: Plot options
   */
  template <typename Real>
  inline void plot(const std::vector<Real> &x, const std::vector<Real> &y,
                   const std::string &opts = "");

  template <typename Scalar, int Rows, int Cols, int Options>
  inline void plot(const Eigen::Matrix<Scalar, Rows, Cols, Options> &matrix);

  /**
   * \brief Plot matrix
   *
   * \tparam Real: Real number
   * \param matrix: Matrix data
   * \param cols: Number of columns
   */
  template <typename Real>
  inline void matrix(const std::vector<Real> &matrix, size_t cols);

  template <typename Real, size_t Size>
  inline void matrix(const std::array<Real, Size> &matrix, size_t cols);

  /**
   * \brief
   *
   * \tparam Real: Real number
   * \param filename: Filename with data
   * \param binwidth: Histogram bin width
   * \param opts: Plot options
   */
  template <typename Real>
  inline void hist(const std::string &filename, const Real &binwidth,
                   const std::string &opts = "");

}; // Gnuplot

// - Enter values
//
void Gnuplot::beginPoints(const std::string &opts) {
  std::ostringstream command;
  command << "plot \'-\' " << opts;
  enterCommand(command.str());
}

void Gnuplot::beginPoints(size_t n_series, const std::string &opts) {
  std::ostringstream command;
  command << "plot \'-\' lc " << 1 << " " << opts;
  for (size_t i = 2; i <= n_series; i++)
    command << ", \'-\' lc " << i << " " << opts;
  enterCommand(command.str());
}

void Gnuplot::beginPoints(size_t n_series,
                          const std::initializer_list<std::string> &opts) {
  ASSERT(opts.size() == n_series)
  std::vector<std::string> opts_vec(opts);
  std::ostringstream command;
  command << "plot \'-\' " << opts_vec[0];
  for (size_t i = 1; i < n_series; i++)
    command << ", \'-\' " << opts_vec[i];
  enterCommand(command.str());
}

void Gnuplot::endPoints() { enterCommand("e"); }

template <typename Real0, typename Real1>
void Gnuplot::point2D(const Real0 &v0, const Real1 &v1) {
  fprintf(pipe_, "%f\t%f\n", v0, v1);
  fflush(pipe_);
}

template <typename Real0, typename Real1, typename Real2>
void Gnuplot::point3D(const Real0 &v0, const Real1 &v1, const Real2 &v2) {
  fprintf(pipe_, "%f\t%f\t%f\n", v0, v1, v2);
  fflush(pipe_);
}

// - Plot data directly
template <typename Real>
void Gnuplot::plot(const std::vector<Real> &x, const std::vector<Real> &y,
                   const std::string &opts) {
  ASSERT(x.size() == y.size())

  beginPoints(1, opts);
  for (size_t i = 0; i < x.size(); i++)
    point2D(x[i], y[i]);
  endPoints();
}

template <typename Scalar, int Rows, int Cols, int Options>
void Gnuplot::plot(const Eigen::Matrix<Scalar, Rows, Cols, Options> &matrix) {
  int n_rows = matrix.rows();
  int n_cols = matrix.cols();

  enterCommand("set key off");
  enterCommand("set palette grey");
  enterCommand(concatString("set xrange [-1:", n_cols, "]"));
  enterCommand(concatString("set yrange [", n_rows, ":-1]"));
  enterCommand("plot \'-\' matrix with image");

  for (int i = 0; i < n_rows; i++) {
    std::ostringstream command;
    command << matrix.row(i);
    // for (int j = 0; j < n_cols; j++)
    //     command << matrix(i, j) << "\t";

    enterCommand(command.str());
  }

  endPoints();
}

// - Matrix

template <typename Real>
void Gnuplot::matrix(const std::vector<Real> &matrix, size_t cols) {

  enterCommand("set palette grey");
  enterCommand("plot \'-\' matrix with image");

  std::ostringstream command;
  for (size_t i = 0; i < matrix.size(); i++) {
    command << matrix[i] << "\t";
    if (((i + 1) % cols) == 0)
      command << "\n";
  }
  enterCommand(command.str());

  endPoints();
}

template <typename Real, size_t Size>
void Gnuplot::matrix(const std::array<Real, Size> &matrix, size_t cols) {

  enterCommand("set palette grey");
  enterCommand("plot \'-\' matrix with image");

  std::ostringstream command;
  for (size_t i = 0; i < Size; i++) {
    command << matrix[i] << "\t";
    if (((i + 1) % cols) == 0)
      command << "\n";
  }
  enterCommand(command.str());

  endPoints();
}

// - Histograms

template <typename Real>
void Gnuplot::hist(const std::string &filename, const Real &binwidth,
                   const std::string &opts) {
  std::ostringstream command;

  command << "binwidth=" << binwidth;
  enterCommand(command.str());
  command.str("");

  enterCommand("set boxwidth binwidth");
  enterCommand("bin(x,width)=width*floor(x/width) + width/2.0");

  command << "plot \'" << filename
          << "\' using (bin($1,binwidth)):(1.0) smooth freq with boxes" << opts;
  enterCommand(command.str());
}
