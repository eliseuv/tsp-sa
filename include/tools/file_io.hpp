#pragma once

/**
 * \file file_io.hpp
 * \author eliseuv (eliseuv816@gmail.com)
 * \brief Tools to Input/Output data
 * 
 */

#include "core.hpp"

namespace io {

    // # Input

    /// Count number of lines and rows in a file
    std::pair<size_t, size_t> getMatrixShape(const std::string& filename,
                                             const std::string& delim=",");
    
    template<typename M>
    M loadMatrixCSV (const std::string& path) {
        std::ifstream indata;
        indata.open(path);
        std::string line;
        std::vector<double> values;
        uint rows = 0;
        while (std::getline(indata, line)) {
            std::stringstream lineStream(line);
            std::string cell;
            while (std::getline(lineStream, cell, ',')) {
                values.push_back(std::stod(cell));
            }
            ++rows;
        }
        return Eigen::Map<const Eigen::Matrix<typename M::Scalar, M::RowsAtCompileTime, M::ColsAtCompileTime, Eigen::RowMajor>>(values.data(), rows, values.size()/rows);
    }

    // # Output

    /// Set numerical precision
    template <typename Floating>
    std::_Setprecision floatingPrecision = std::setprecision(std::numeric_limits<Floating>::max_digits10 + 2);

    // - CSV

    /**
     * \brief Write comma separated values (CSV) to output stream\n
     * Final call.
     */
    template <typename T>
    void csvStream(std::ostream& output_stream, T last_value) {
        output_stream << last_value << std::endl;
    }

    /// Write comma separated values (CSV) to output stream
    template <typename T, typename... Ts>
    void csvStream(std::ostream& output_stream, T value, Ts... other_values) {
        output_stream << value << ",";
        csvStream(output_stream, other_values...);
    }

    
    inline std::string filename(std::initializer_list<std::string> name_parts) {
        std::stringstream ss;
        std::initializer_list<std::string>::iterator it;
        for (it = name_parts.begin(); it != name_parts.end()-1; ++it)
            ss << *it << ",";
        ss << *it;
        return ss.str();
    }

    template <typename T>
    void printVector(const std::vector<T>& vec, std::ostream& output_stream=std::cout) {
        for (const T& x : vec) output_stream << x << ",";
        output_stream << std::endl;
    }

} // io