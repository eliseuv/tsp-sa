#pragma once

/**
 * \file explorer.hpp
 * \author eliseuv (eliseuv816@gmail.com)
 * \brief 
 * 
 */

#include "graphs/complete_weighted_graph.hpp"
#include "graphs/hamiltonian_cycle.hpp"
#include "sampling/markov.hpp"

namespace graph {

// # Graph Explorer

/**
 * \brief 
 * 
 */
class GraphExplorer : public MCMC<std::vector<size_t>, Real>
{
public:

    using State = std::vector<size_t>;
    using NodeDist = std::uniform_int_distribution<size_t>;

private:

    const KWGraph *const kwgraph_;

    NodeDist dist_;
    State state_, inv_state_;
    State::iterator next_node_;

    Real cost_;

    bool closed_cycle_;

public:

    // - Constructor

    GraphExplorer(const KWGraph& kwgraph, size_t initial_node=0);

    // - Current state

    inline const size_t& getCurrentNode() const { return state_.back(); }
    inline const size_t& getNextNode() const    { return *next_node_; }

    // - States not visited

    inline const State& not_visited() const  { return inv_state_; }

    // - Markov Chain

    virtual inline const State& getCurrentState() const final        { return state_; }
    virtual void setCurrentState(const State& state) final;

    virtual inline void selectNextState(MT19937& gen) final {
                                                                if (persistence() == 0) return;
                                                                next_node_ = (inv_state_.begin() + dist_(gen));
                                                            }
    virtual inline void jumpNextState() final   {
                                                    if (persistence() == 0) return;
                                                    state_.push_back(*next_node_);
                                                    inv_state_.erase(next_node_);
                                                    next_node_ = inv_state_.begin();
                                                    dist_.param(NodeDist::param_type(0, inv_state_.size()-1));
                                                }

    // - MCMC

    virtual inline Real getCurrentValue() const final   { return cost_; }
    virtual inline Real getLocalValue() const final     { return kwgraph_->weight(getCurrentNode(), getNextNode()); }

    virtual void calculateCurrentValue() final;
    virtual inline void updateValueThenJumpNextState() final    { cost_ += getLocalValue(); jumpNextState(); }

    // - Persistence

    inline size_t visitedNodesCount() const { return state_.size(); }
    inline size_t persistence() const       { return inv_state_.size(); }
    inline Real norm_persistence() const    { return static_cast<Real>(persistence())/kwgraph_->nodeCount(); }

    // - Deterministic selection

    void selectNearestNeighbour();

    // virtual inline void selectDeterministicNextState() final { selectNearestNeighbour(); }

    // - Close cycle

    /**
     * \brief Closes cycle if all nodes have been visited.
     * 
     * \return true Cycle successfully closed or already closed.
     * \return false Not all nodes have been visited.
     */
    bool closeCycle();

    inline HCycle getHamiltonianCycle() const { if (closed_cycle_) return HCycle(*kwgraph_, state_); }
    
    // - Print

    void print(std::ostream& output_stream) const;
    void printDebug(std::ostream& output_stream) const;

}; // GraphExplorer

} // graph