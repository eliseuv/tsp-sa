#pragma once

/**
 * \file complete_weighted_graph.hpp
 * \author Eliseu Venites Filho (eliseuv816@gmail.com)
 * \brief Basically a wrapper around an Eigen matrix
 * 
 */

#include "core.hpp"
#include "maths/topology.hpp"

namespace graph {

class KWGraph
{
public:

    using WeightMatrix = Eigen::Matrix<Real, Eigen::Dynamic, Eigen::Dynamic>;

private:

    WeightMatrix weight_matrix_;

public:

    inline KWGraph(WeightMatrix&& weight_matrix)
    : weight_matrix_(std::move(weight_matrix))
    { ASSERT(weight_matrix_.cols() == weight_matrix_.rows()) }

    inline size_t nodeCount() const { return weight_matrix_.cols(); }
    inline size_t edgeCount() const { return (nodeCount()*(nodeCount()-1))/2; }

    inline const WeightMatrix& weightMatrix() const { return weight_matrix_; }

    inline const Real& weight(size_t u, size_t v) const { return weight_matrix_(u, v); }

    void printWeightMatrix(std::ostream& output_stream) const {
        for (size_t i = 0; i < nodeCount(); i++) {
            for (size_t j = 0; j < nodeCount(); j++) {   
                output_stream << weight_matrix_(i, j);
                if (j != nodeCount()-1) output_stream << ",";
            }
            output_stream << std::endl;
        }
    }
    void printWeightList(std::ostream& output_stream) const {
        for (size_t i = 0; i < nodeCount(); i++) {
            for (size_t j = i+1; j < nodeCount(); j++) {   
                output_stream << weight_matrix_(i, j) << std::endl;
            }
        }
    }

    // void printRaw(std::ostream& output_stream) const {
    //     for (auto it = weight_matrix_.data(); it != weight_matrix_.data() + weight_matrix_.size(); it++) {
    //         std::cout << *it << std::endl;
    //     }
    // }

}; // KWGraph

} // graph