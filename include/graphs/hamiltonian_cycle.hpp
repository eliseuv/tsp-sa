#pragma once

/**
 * \file hamiltonian_cycle.hpp
 * \author Eliseu Venites Filho (eliseuv816@gmail.com)
 * \brief Hamiltonian Cycle on a complete weighted graph
 *
 */

#include "core.hpp"
#include "graphs/complete_weighted_graph.hpp"
#include "maths/permutation.hpp"
#include "maths/stochastic.hpp"
#include "maths/topology.hpp"
#include "sampling/markov.hpp"
#include "tools/file_io.hpp"

namespace graph {

template <typename PositiveReal>
inline Real
defaultHCycleCost(const topo::FiniteMetricSpace<PositiveReal> &metric_space) {
  const size_t N = metric_space.elementCount();
  PositiveReal cost = metric_space.metric(N - 1, 0);
  for (size_t i = 0; i < N - 1; i++)
    cost += metric_space.metric(i, i + 1);
  return cost;
}

// # Nearest Neighbour Cycle

std::pair<Permutation::State, Real>
nearestNeighbourCycle(const topo::FiniteMetricSpace<Real> &metric_space,
                      size_t initial_node = 0);

std::pair<Permutation::State, Real>
nearestNeighbourCycle(const graph::KWGraph &kwgraph, size_t initial_node);
std::pair<Permutation::State, Real>
nearestNeighbourCycleMin(const graph::KWGraph &kwgraph);

// # Hamiltonian Cycle

class HCycle : public Permutation {
public:
  using State = Permutation::State;

private:
  const KWGraph *const kwgraph_; ///< Graph associated
  Real cost_;                    ///< Cost of the cycle

public:
  inline HCycle(const KWGraph &kwgraph, const State &state)
      : Permutation(kwgraph.nodeCount()), kwgraph_(&kwgraph) {
    setState(state);
    calculateCost();
  }

  inline HCycle(const KWGraph &kwgraph)
      : Permutation(kwgraph.nodeCount()), kwgraph_(&kwgraph) {
    calculateCost();
  }

  inline const KWGraph &kwgraph() const { return *kwgraph_; }

  inline const Real &weight(size_t u, size_t v) const {
    return kwgraph_->weight(this->state(u), this->state(v));
  }

  inline void reset() {
    Permutation::reset();
    calculateCost();
  }
  inline void shuffle(MT19937 &gen) {
    Permutation::shuffle(gen);
    calculateCost();
  }
  void shuffle() {
    MT19937 gen((std::random_device()()));
    shuffle(gen);
  }
  void nearest_neighbour(size_t initial_node = 0);
  void nearest_neighbour_min();

  inline const Real &cost() const { return cost_; }

  void calculateCost();

  void print(std::ostream &output_stream) const;
  void plot(const topo::EuclideanSpace &space, Gnuplot &gplt,
            const std::string &opts = "with linespoint") const;

  class MCMC;

  class SimpleSwap;
  // class Swap;
  class Twist;

}; // HCycle

// # MCMC

class HCycle::MCMC : public ::MCMC<State, Real> {
protected:
  HCycle *const hcycle_; ///< Hamiltonian Cycle associated
  Real local_value_;

  inline MCMC(HCycle &hcycle) : hcycle_(&hcycle) {}

public:
  constexpr const HCycle &hcycle() const { return *hcycle_; }

public:
  // - Markov Chain

  virtual inline const State &getCurrentState() const final {
    return hcycle_->getState();
  }
  virtual inline void setCurrentState(const State &state) final {
    hcycle_->setState(state);
  }

  // - MCMC

  virtual inline Real getCurrentValue() const final { return hcycle_->cost(); }
  virtual inline Real getLocalValue() const final { return local_value_; }

  virtual inline void calculateCurrentValue() final {
    hcycle_->calculateCost();
  }
  virtual inline void updateValueThenJumpNextState() final {
    hcycle_->cost_ += local_value_;
    jumpNextState();
  }

}; // HCycle::MCMC

// # SimpleSwap

class HCycle::SimpleSwap : public MCMC {
private:
  size_t i_, j_;

  std::uniform_int_distribution<size_t> state_dist_;

public:
  inline SimpleSwap(HCycle &hcycle)
      : MCMC(hcycle), i_(0), j_(1), state_dist_(0, hcycle.deg() - 1) {
    this->local_value_ = 0.0;
  }

  virtual inline void selectNextState(MT19937 &gen) final {
    const size_t N = this->hcycle_->deg();

    i_ = state_dist_(gen);
    j_ = ((i_ == N - 1) ? 0 : (i_ + 1));

    this->local_value_ = 0.0;

    // Periodic boundaries
    size_t i_prev = ((i_ == 0) ? (N - 1) : (i_ - 1));
    size_t j_next = ((j_ == N - 1) ? (0) : (j_ + 1));

    // Old edges: [i_prev]--[i] and [j]--[j_next]
    this->local_value_ -= this->hcycle_->weight(i_prev, i_);
    this->local_value_ -= this->hcycle_->weight(j_, j_next);
    // New edges: [i_prev]--[j] and [i]--[j_next]
    this->local_value_ += this->hcycle_->weight(i_prev, j_);
    this->local_value_ += this->hcycle_->weight(i_, j_next);
  }

  virtual inline void jumpNextState() final {
    std::swap(this->hcycle_->state_[i_], this->hcycle_->state_[j_]);
  }

}; // HCycle::SimpleSwap

// # Swap

// class HCycle::Swap: public MCMC
// {
// private:

//     size_t i_, j_;

//     std::uniform_int_distribution<size_t> first_state_dist_;
//     std::uniform_int_distribution<size_t> other_state_dist_;

// public:

//     inline Swap(HCycle& hcycle)
//     : MCMC(hcycle),
//       i_(0), j_(1),
//       first_state_dist_(0, hcycle.deg()-1),
//       other_state_dist_(1, hcycle.deg()-1)
//     { this->local_value_ = 0.0; }

//     virtual inline void selectNextState(MT19937& gen) final {
//         const size_t N = this->hcycle_->deg();

//         i_ = first_state_dist_(gen);
//         j_ = (i_ + other_state_dist_(gen)) % N;

//         this->local_value_ = 0.0;

//         // Periodic boundaries
//         size_t i_prev = (i_-1)%N, i_next = (i_+1)%N;
//         size_t j_prev = (j_-1)%N, j_next = (j_+1)%N;

//         // Check if selected nodes are neighbours
//         if (i_next == j_) {
//             // Old edges: [i_prev]--[i] and [j]--[j_next]
//             this->local_value_ -= this->hcycle_->weight(i_prev, i_);
//             this->local_value_ -= this->hcycle_->weight(j_, j_next);
//             // New edges: [i_prev]--[j] and [i]--[j_next]
//             this->local_value_ += this->hcycle_->weight(i_prev, j_);
//             this->local_value_ += this->hcycle_->weight(i_, j_next);
//         }
//         else if (j_next == i_) {
//             // Old edges: [j_prev]--[j] and [i]--[i_next]
//             this->local_value_ -= this->hcycle_->weight(j_prev, j_);
//             this->local_value_ -= this->hcycle_->weight(i_, i_next);
//             // New edges: [j_prev]--[i] and [j]--[i_next]
//             this->local_value_ += this->hcycle_->weight(j_prev, i_);
//             this->local_value_ += this->hcycle_->weight(j_, i_next);
//         }
//         else {
//             // Old edges: [j_prev]--[j]--[j_next] and [i_prev]--[i]--[i_next]
//             this->local_value_ -= this->hcycle_->weight(j_prev, j_);
//             this->local_value_ -= this->hcycle_->weight(j_, j_next);
//             this->local_value_ -= this->hcycle_->weight(i_prev, i_);
//             this->local_value_ -= this->hcycle_->weight(i_, i_next);
//             // New edges: [j_prev]--[i]--[j_next] and [i_prev]--[j]--[i_next]
//             this->local_value_ += this->hcycle_->weight(j_prev, i_);
//             this->local_value_ += this->hcycle_->weight(i_, j_next);
//             this->local_value_ += this->hcycle_->weight(i_prev, j_);
//             this->local_value_ += this->hcycle_->weight(j_, i_next);
//         }
//     }

//     virtual inline void jumpNextState() final {
//         size_t temp = this->hcycle_->state_[i_];
//         this->hcycle_->state_[i_] = this->hcycle_->state_[j_];
//         this->hcycle_->state_[j_] = temp;
//     }

// };// HCycle::Swap

// # Twist

class HCycle::Twist : public MCMC {
private:
  size_t i_, j_;

  std::uniform_int_distribution<size_t> first_state_dist_;
  std::uniform_int_distribution<size_t> other_state_dist_;

public:
  inline Twist(HCycle &hcycle)
      : MCMC(hcycle), i_(0), j_(hcycle.deg() - 2),
        first_state_dist_(0, hcycle.deg() - 1),
        other_state_dist_(1, std::floor(hcycle.deg() / 2.0) - 1) {
    this->local_value_ = 0.0;
  }

  virtual inline void selectNextState(MT19937 &gen) final {
    const size_t N = this->hcycle_->deg();

    i_ = first_state_dist_(gen);
    j_ = (i_ + other_state_dist_(gen)) % N;

    this->local_value_ = 0.0;

    // Periodic boundaries
    size_t h = ((i_ == 0) ? (N - 1) : (i_ - 1));
    size_t k = ((j_ == N - 1) ? (0) : (j_ + 1));

    // Old edges: [h]--[i] and [j]--[k]
    this->local_value_ -= this->hcycle_->weight(h, i_);
    this->local_value_ -= this->hcycle_->weight(j_, k);
    // New edges: [k]--[i] and [j]--[h]
    this->local_value_ += this->hcycle_->weight(k, i_);
    this->local_value_ += this->hcycle_->weight(j_, h);
  }

  virtual inline void jumpNextState() final {
    if (i_ < j_)
      std::reverse(this->hcycle_->state_.begin() + i_,
                   this->hcycle_->state_.begin() + (j_ + 1));
    else
      std::reverse(this->hcycle_->state_.begin() + (j_ + 1),
                   this->hcycle_->state_.begin() + i_);
  }

}; // HCycle::Twist

} // namespace graph
