#pragma once

// - Precompiled header
#include "pch.hpp"

// - Core definitions
#include "core.hpp"

// - Maths
#include "maths/numbers.hpp"
#include "maths/basic_math.hpp"
#include "maths/permutation.hpp"
#include "maths/topology.hpp"
#include "maths/statistics.hpp"
#include "maths/stochastic.hpp"

// - Graphs
#include "graphs/complete_weighted_graph.hpp"
#include "graphs/hamiltonian_cycle.hpp"
#include "graphs/graph_explorer.hpp"

// - Sampling
#include "sampling/markov.hpp"
#include "sampling/markov_sampler.tpp"
#include "sampling/metropolis.tpp"
#include "sampling/annealer.tpp"