#pragma once

/**
 * \file basic_math.hpp
 * \author Eliseu Venites Filho (eliseuv816@gmail.com)
 * \brief Basic mathematical definitons and functions.
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#include "numbers.hpp"

// # Macro defined functions

/// Square operation macro
#define SQ(x) ((x)*(x))

#define SIGN(x) ( ((x) < 0) ? (-1) : (+1) )

#define ABS(x) ( ((x) < 0) ? (-x) : (x) )

// # Constant expression template functions

/// Sum multiple values using fold expression
template <typename... Ts>
constexpr auto sum(Ts... args) {
    return (args + ...);
}

/// Multiply multiple values using fold expression
template <typename... Ts>
constexpr auto product(Ts... args) {
    return (args * ...);
}

template <typename T>
constexpr T square(T x) {
    return (x*x);
}

template <typename T>
constexpr T abs(T x) {
    if constexpr (x < 0) return (-x);
    else return x;
}

template <typename I>
constexpr Real div(I a, I b) {
    return (static_cast<Real>(a)/b);
}

// # NumPy like functions

inline std::vector<Real> logspace(Real min, Real max, size_t num, Real base=10) {
    std::vector<Real> values(num);
    Real c = std::pow(base, (max-min)/(num-1.0));
    values[0] = std::pow(base,min);
    for (size_t i = 1; i < values.size(); i++)
        values[i] = c*values[i-1];
    return values;
}

/**
 * \brief Generalized logarithm
 * 
 * \f$ \log_{\alpha} = \int_1^x dx' x'^{\alpha-1} \f$
 * 
 * for \f$ \alpha = 0 \f$:
 * \f$ \log_0(x) = \ln(x) \f$
 * 
 * for \f$ \alpha \neq 0 \f$:
 * \f$ \log_{\alpha}(x) = \frac{x^\alpha - 1}{\alpha} \f$
 */
inline Real genLog(const Real& alpha, const Real& x)
{
if (alpha == 0)
    return std::log(x);
else
    return (std::pow(x, alpha)-1.0)/(alpha);
}

/**
 * \brief Generalized exponentiation
 * 
 * The generalized exp function is defined as the inverse of the generalized logarithm function
 * 
 * for \f$\alpha x < -1 \f$:
 * \f$ \exp_{\alpha}(x) = 0 \f$
 * 
 * for \f$ \alpha x \geq -1 \f$:
 * \f$ \exp_{\alpha}(x) = \lim_{\alpha'\rightarrow\alpha} (1 + \alpha' x)^{1/\alpha'} \f$
 * 
 * for \f$ \alpha = 0 \f$:
 * \f$ \exp_0(x) = \exp(x) \f$
 */
inline Real genExp(const Real& alpha, const Real& x)
{
if (alpha == 0)
    return std::exp(x);
if (alpha*x > -1.0)
    return std::pow(1.0+alpha*x, 1.0/alpha);
else
    return 0.0;
}

/**
 * \brief Enforce periodic boundary conditions on a Real value.\n
 * The period considered is \f$[0,p)\f$.\n
 * `periodic(2.3, 5.5) = 2.3`\n
 * `periodic(6.7, 5.5) = 1.2`\n
 * `periodic(12.5, 5.5) = 1.5`\n
 * `periodic(-1.5, 5.5) = 3.0`
 * 
 * \param value: Real value
 * \param period: Upper limit of the period considered
 * \return Real: Value inside the period
 */
inline Real periodic(const Real& value, const Real& period)
{
    Real result = std::fmod(value, period);
    if (result < 0) result += period;
    return result;
}

/**
 * \brief Haversine (half of versine) function.\n
 * Versine (or versed sine) is defined as \f$ \text{versin}(\theta) = 1 - \cos(\theta) = 2\sin^2\left(\frac{\theta}{2}\right)\f$\n
 * \f[\text{hav}(\theta) = \frac{1}{2}\text{versin}(\theta) = sin^2\left(\frac{\theta}{2}\right)\f]
 */
inline Real haversine(const Real& theta)
{
    Real s = std::sin(theta/2);
    return SQ(s);
}