#pragma once

/**
 * \file stochastic.hpp
 * \author eliseuv (eliseuv816@gmail.com)
 * \brief PRNGs and distributions.
 *
 */

#include "maths/basic_math.hpp"

// # Mersenne Twister Type

/// Type alias for STL Mersenne Twister RNG
/**
 * if `Real` is `float` use `std::mt19937`
 *
 * else if `Real` is `double` use `std::mt19937_64`
 */
using MT19937 = std::conditional_t<std::is_same<Real, float>::value,      //  if (T is single precision)
                    typename std::mt19937,                                //      use mt19937
                                                                          //  else
                    std::conditional_t<std::is_same<Real, double>::value, //      if (T is double precision)
                        typename std::mt19937_64,                         //          use mt19937_64
                        void>                                             //      else use void
                >; // MT19937

/// Random value generated from random device to be used as seed
inline auto randomSeed() {
    std::random_device rdv;
    return rdv();
}

// # Uniform Distribtution Type

/// Type alias for STL uniform distribution
/**
 * if `T` is integral (`int`, `size_t`, `long`, ...) use `std::uniform_int_distribution<T>`
 *
 * else if `T` is floating point (`float` or `double`) use `std::uniform_real_distribution<T>`
 */
template <typename T>
using UniformDistribution = std::conditional_t<std::is_integral<T>::value,           //  if (T is integral)
                                std::uniform_int_distribution<T>,                    //      use uniform_int_distribution
                                                                                     //  else
                                std::conditional_t<std::is_floating_point<T>::value, //      if (T is floating point)
                                    std::uniform_real_distribution<T>,               //          use uniform_real_distribution
                                    void>                                            //      else use void
                            >;

// # Real Distribution

/// Interface that allows Real distributions to passed as parameters
class RealDistribution
{
public:
    /// Generate value
    virtual Real operator()(MT19937& gen) = 0;

}; // RealDistribution

// # Uniform Real Distribution

/// Wrapper around the STL uniform distribution
/** \f$ X \in [x_{lower},x_{upper}] \f$ */
class UniformRealDistribution : public RealDistribution
{
protected:

    std::uniform_real_distribution<Real> u_dist_;   ///< STL Uniform Distribution

public:

    /// Constructor given lower and upper bounds
    inline UniformRealDistribution(const Real& lower_bound=0.0, const Real& upper_bound=1.0)
    : RealDistribution(), u_dist_(lower_bound, upper_bound) {}

    /// Generate value
    virtual inline Real operator()(MT19937& gen) override
    { return u_dist_(gen);}

}; // UniformRealDistribution

// # Normal Distribution

/// Wrapper around the STL normal distribution
/** \f[ p(x) = \frac{1}{\sigma\sqrt{2\pi}}\exp(-\frac{1}{2}(\frac{x-\mu}{\sigma})^2) \f] */
class NormalDistribution : public RealDistribution
{
protected:

    std::normal_distribution<Real> normal_dist_;    ///< STL Normal Distribution

public:

    /**
     * \brief Constructor given mean and standard deviation
     *
     * \param mean: Mean \f$\mu\f$
     * \param stddev: Standard Deviation \f$\sigma\f$
     */
    inline NormalDistribution(const Real& mean=0.0, const Real& stddev=1.0)
    : normal_dist_(mean, stddev) {}

    /// Generate value
    virtual inline Real operator()(MT19937& gen) override
    { return normal_dist_(gen); }

}; // NormalDistribution

// # Pareto Distribution

/// Pareto distribution
/** \f[ p(x) = \frac{\alpha x_m^{\alpha}}{x^{\alpha+1}} \f] */
class ParetoDistribution : public RealDistribution
{
private:

    std::uniform_real_distribution<Real> udist_{0, 1};  ///< STL Uniform Distribution
    Real shape_;                                        ///< Shape \f$\alpha\f$
    Real ninv_shape_;                                   ///< \f$=1/\alpha\f$
    Real scale_;                                        ///< Scale \f$x_m\f$

public:

    /**
     * \brief Construct a new Pareto Distribution object
     *
     * \param shape: Shape \f$\alpha\f$
     * \param scale: Scale \f$x_m\f$
     */
    inline ParetoDistribution(const Real& shape, const Real& scale=1)
    : shape_(shape), ninv_shape_(-1.0/shape), scale_(scale)
    { ASSERT((shape > 0) && (scale > 0)) }

    /// Set shape
    inline void shape(const Real shape)   { ASSERT(shape > 0) shape_=shape; ninv_shape_=-1.0/shape; }
    /// Get shape
    inline Real shape() const             { return shape_; }
    /// Set scale
    inline void scale(const Real scale)   { ASSERT(scale > 0) scale_=scale; }
    /// Get scale
    inline Real scale() const             { return scale_; }

    /// Generate value
    inline virtual Real operator()(MT19937& gen) override {
        Real u = udist_(gen);
        return (scale_*std::pow<Real>(1-u, ninv_shape_));
    }

}; // ParetoDistribution

// # Beta Distribution

/// Beta distribution
/**
 * \f[ p(x) = \frac{x^{\alpha-1}(1-x)^{\beta-1}}{B(\alpha,\beta)} \f]
 *
 * where \f$ \displaystyle B(\alpha,\beta) = \frac{\Gamma(\alpha)\Gamma(\beta)}{\Gamma(\alpha+\beta)} \f$
 * and \f$ \displaystyle \Gamma(z) = \int_0^\infty dx x^{z-1}e^{-x} \f$
 */
class BetaDistribution : public RealDistribution
{
private:

    std::gamma_distribution<Real> x_gamma_dist_, y_gamma_dist_;

public:

    /**
     * \brief Construct a new Beta Distribution object
     *
     * \param alpha: Shape alpha \f$\alpha\f$
     * \param beta: Shape beta \f$\beta\f$
     */
    inline BetaDistribution(const Real& alpha=1, const Real& beta=1)
    : x_gamma_dist_(alpha, 1.0), y_gamma_dist_(beta, 1.0)
    { ASSERT((alpha > 0) && (beta > 0)) }

    /// Generate value
    inline virtual Real operator()(MT19937& gen) override {
        Real x = x_gamma_dist_(gen);
        Real y = y_gamma_dist_(gen);
        return (x/(x+y));
    }

}; // BetaDistribution

// # Power Law Distribution

/// Power Law distribution
/**
 * \f[
 * p(x;x_0,\alpha) =
 * \begin{cases}
 * \displaystyle \frac{\alpha-1}{x_0^{1-\alpha}} \quad &\text{if } x \geq x_0 \\
 * 0 \quad &\text{otherwise}
 * \end{cases}
 * \f]
 */
class PowerLawDistribution : public RealDistribution
{
private:

    Real x_0_;      ///< Minimum value \f$x_0\f$
    Real alpha_;    ///< Coefficient \f$\alpha\f$
    Real beta_;     ///< \f$ \beta = \frac{1}{1-\alpha} \f$
    /// Uniform distribution \f$ x \in [0,1] \f$
    std::uniform_real_distribution<Real> u_dist_;

public:

    /// Constructor
    inline PowerLawDistribution(const Real& alpha, const Real& x_0)
    : x_0_(x_0), alpha_(alpha), beta_(1.0/(1.0-alpha)),
      u_dist_(0.0, 1.0)
    { ASSERT((alpha > 0) && (x_0 > 0)) }

    /// Get alpha
    inline Real alpha() const { return alpha_; }
    /// Get minimum value
    inline Real x_0() const { return x_0_; }

    /// Generate value
    inline virtual Real operator()(MT19937& gen) override {
        return (x_0_*std::pow(u_dist_(gen), beta_));
    }

}; // PowerLawDistribution

class SymmetricPowerLawDistribution : public PowerLawDistribution
{
private:

    std::bernoulli_distribution coin_flip;

public:

    inline SymmetricPowerLawDistribution(const Real& alpha, const Real& x_0)
    : PowerLawDistribution(alpha, x_0),
      coin_flip()
    {}

    /// Generate value
    inline virtual Real operator()(MT19937& gen) override {
        Real plaw = PowerLawDistribution::operator()(gen);
        if (coin_flip(gen)) return plaw;
        else return -plaw;
    }

};

// # Correlated Pairs Source

/// Source of pairs of correlated random variables
class CorrelatedPairSource
{
private:
    Real rho_;
    Real a_, b_;

public:
    inline CorrelatedPairSource(Real rho=0.0)
    { setCorrelation(rho); }

    inline void setCorrelation(Real rho) {
        rho_ = rho;
        Real phi = 0.5*std::asin(rho);
        a_ = std::cos(phi);
        b_ = std::sin(phi);
    }

    inline std::pair<Real, Real> operator()(MT19937& gen, RealDistribution& dist) {
        Real x0 = dist(gen);
        Real x1 = dist(gen);
        return std::make_pair(a_*x0 + b_*x1,
                              b_*x0 + a_*x1);
    }
};

// # Random Selector

/// Random selector
template <class RandomGenerator=MT19937>
struct RandomSelector
{
	template <typename Iter>
	Iter select(Iter start, Iter end, RandomGenerator& gen) {
		std::uniform_int_distribution<size_t> u_dist(0, std::distance(start, end) - 1);
		std::advance(start, u_dist(gen));
		return start;
	}

	template <typename Iter>
	Iter operator()(Iter start, Iter end, RandomGenerator& gen) {
		return select(start, end, gen);
	}

    template <typename Container>
	typename Container::iterator choose(Container& c, RandomGenerator& gen) {
		return select(begin(c), end(c), gen);
	}

	//convenience function that works on anything with a sensible begin() and end(), and returns with a ref to the value type
	template <typename Container>
	auto operator()(const Container& c, RandomGenerator& gen) -> decltype(*begin(c))& {
		return *select(begin(c), end(c), gen);
	}
};