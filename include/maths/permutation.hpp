#pragma once

/**
 * \file permutation.hpp
 * \author eliseuv (eliseuv816@gmail.com)
 * \brief Permutation group
 * 
 */

#include "core.hpp"
#include "maths/numbers.hpp"
#include "maths/stochastic.hpp"

// # Permutation

class Permutation
{
public:

    /// Permutation state type
    using State     = std::vector<size_t>;

    inline static State defaultState(size_t N) {
        State s(N);
        for (size_t i = 0; i < N; i++) s[i] = i;
        return s;
    }

protected:

    /// Permutation state
    State state_;

    /// Set permutation state
    inline void setState(const State& state) { ASSERT(state.size() == state_.size()) state_ = state; }

public:

    // - Constructors

    inline Permutation()                : state_(0) {}                  ///< Null permutation \f$ (deg = 0) \f$
    inline Permutation(size_t N) : state_(N) { reset(); }               ///< Neutral element
    inline Permutation(State&& state)   : state_(std::move(state)) {}

    // - State

    inline size_t deg() const                        { return state_.size(); }                  ///< Get permutation degree
    inline const State& getState() const             { return state_; }                         ///< Get current state
    inline const size_t& state(size_t i) const       { ASSERT(i < deg()) return state_[i]; }    ///< Get i-th permutation position
    inline const size_t& operator()(size_t i) const  { return state(i); }                       ///< Get i-th permutation position

    /// Check if a given state is a valid permutation
    static bool check(const State& state);
    /// Check if the current state is a valid parmutation
    inline bool check() const               { return check(this->state_); }

    // - Set state

    /// Reset permutation to default
    /** \f$ \pi = (0, 1, ..., N) \f$ */
    inline void reset()                 { for (size_t i = 0; i < state_.size(); i++) state_[i] = i; }
    /// Shuffle permutation using a given random number generator
    inline void shuffle(MT19937& gen)   { std::shuffle(state_.begin(), state_.end(), gen); }
    /// Shuffle permutation
    inline void shuffle()               { MT19937 gen((std::random_device()())); shuffle(gen); }

    // - Swap entries

    inline void swap(size_t u, size_t v)    { ASSERT((u < deg()) && (v < deg())) std::swap(state_[u], state_[v]); }

    // - Equality

    /// Check if two permutations are the same
    bool operator==(const Permutation& other) const;

    // - Composition: G x G -> G

    /// Composition of permutations \f$ G \times G \rightarrow G \f$
    /** Left first convention: \f$ \pi\sigma(x) = \pi(\sigma(x)) \f$ */
    Permutation operator*(const Permutation& other) const;
    
    // - Print

    /// Print permutation to output stream
    void printState(std::ostream& output_stream) const;

}; // Permutation
