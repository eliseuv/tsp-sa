#pragma once

/**
 * \file statistics.hpp
 * \author eliseuv (eliseuv816@gmail.com)
 * \brief Statistics on variables
 * 
 */

#include "maths/basic_math.hpp"
#include "tools/gnuplot.hpp"

// # Statistics

namespace stats {

    // # Statistics on vectors

    inline Real vectorAverage(const std::vector<Real>& vec, size_t N=0) {
        if (vec.size() < 1) return 0;
        if ((N == 0) || (N > vec.size())) N = vec.size();
        Real sum = 0;
        for (size_t i = 0; i < N; i++) sum += vec[i];
        return sum/N;
    }

    inline Real vectorVariance(const std::vector<Real>& vec, size_t N=0) {
        if (vec.size() < 2) return 0;
        if ((N == 0) || (N > vec.size())) N = vec.size();
        Real sum_sq = 0;
        for (size_t i = 0; i < N; i++) sum_sq += SQ(vec[i]);
        return ((sum_sq/N) - SQ(vectorAverage(vec, N)));
    }

    inline Real vectorStddev(const std::vector<Real>& vec, size_t N=0)
    { return std::sqrt(vectorVariance(vec, N)); }

    inline std::pair<Real,Real> vectorAverageVariance(const std::vector<Real>& vec, size_t N=0) {
        if (vec.size() < 2) return std::make_pair(0, -1);
        if ((N == 0) || (N > vec.size())) N = vec.size();
        Real sum = 0, sum_sq = 0;
        for (size_t i = 0; i < N; i++) {
            sum += vec[i];
            sum_sq += SQ(vec[i]);
        }
        Real average = sum/N;
        Real variance = (sum_sq/N) - SQ(average);
        return std::make_pair(average, variance);
    }

    // # Accumulator

    /**
     * \brief Accumulator base class that exposes basic functionality
     * 
     * \tparam T: Numerical type
     */
    template <typename T>
    class Accumulator
    {
    protected:
    
        size_t count_ = 0;  ///< Number of values already accumulated
    
    public:

        /// Add new value to accumulator
        virtual inline void operator()([[maybe_unused]]const T& new_value) { count_++; }

        /// Clear accumulator state
        virtual inline void clear() { count_ = 0; }

        /// Get number of values accumulated
        inline size_t count() const { return count_; }

    }; // Accumulator

    // # Sum

    class Sum: Accumulator<Real>
    {
    private:

        Real sum_;

    public:

        Sum(Real initial_value=0)
        : sum_(initial_value)
        {}

        /// 
        virtual inline void operator()(const Real& new_value) override {
            sum_ += new_value;
            this->count_++;
        }

        ///
        inline Sum& operator<<(const Real& new_value) {
            sum_ += new_value;
            return *this;
        }

        /// Get sum value
        inline const Real& get() const { return sum_; }
        /// Get sum value
        inline Real operator()() const { return get(); }

        /// Print sum to output stream
        friend std::ostream& operator<<(std::ostream& os, const Sum& sum) {
            os << sum.get();
            return os;
        }


    }; // Sum

    // # Maximum

    class Maximum : public Accumulator<Real>
    {
    protected:

        /// Maximum value
        Real max_value_;

    public:

        /// Default constructor
        inline Maximum() : max_value_(std::numeric_limits<Real>::lowest()) {}

        /// Construct with an input initial value
        inline Maximum(const Real& initial_value) : max_value_(initial_value) {}
        
        /// 
        virtual inline void operator()(const Real& new_value) override {
            if (new_value > max_value_) max_value_ = new_value;
            this->count_++;
        }

        ///
        inline Maximum& operator<<(const Real& new_value) {
            this->operator()(new_value);
            return *this;
        }

        /// Get average value
        inline const Real& get() const { return max_value_; }
        /// Get average value
        inline Real operator()() const { return get(); }

        /// Print average to output stream
        friend std::ostream& operator<<(std::ostream& os, const Maximum& max) {
            os << max();
            return os;
        }

    };

    // # Minimum

    class Minimum : public Accumulator<Real>
    {
    protected:

        /// Minimum value
        Real min_value_;

    public:

        /// Default constructor
        inline Minimum() : min_value_(std::numeric_limits<Real>::max()) {}

        /// Construct with an input initial value
        inline Minimum(const Real& initial_value) : min_value_(initial_value) {}
        
        /// 
        virtual inline void operator()(const Real& new_value) override {
            if (new_value < min_value_) min_value_ = new_value;
            this->count_++;
        }

        ///
        inline Minimum& operator<<(const Real& new_value) {
            this->operator()(new_value);
            return *this;
        }

        /// Get average value
        inline const Real& get() const { return min_value_; }
        /// Get average value
        inline Real operator()() const { return get(); }

        /// Print average to output stream
        friend std::ostream& operator<<(std::ostream& os, const Minimum& min) {
            os << min();
            return os;
        }

    };

    // # Average

    /**
     * \brief Calculates the average value of a sequence accumulatively
     * This class only calculates the first moment
     * For first and second moment use Variance class
     * 
     * \tparam T: Numerical type 
     */
    class Average : public Accumulator<Real>
    {
    protected:

        Real average_;     ///< Average of accumulated values

    public:

        /// brief Construct a new Average object
        inline Average()
        : average_(0)
        {}

        /// Add value to average calculation
        virtual inline void operator()(const Real& new_value) override {
            Accumulator<Real>::operator()(new_value);
            average_ = average_*(static_cast<Real>(this->count_)/(this->count_+1)) + new_value/static_cast<Real>(this->count_+1);
        }

        /**
         * \brief Add value to average calculation
         * \code
         * Average<Real> average(0);
         * average << new_value;
         * \endcode
         */
        inline Average& operator<<(const Real& new_value) {
            this->operator()(new_value);
            return *this;
        }

        /// Clear average calculation
        inline void clear() override { Accumulator<Real>::clear();    average_ = 0.0;}

        /// Get average value
        inline const Real& average() const { return average_; }
        /// Get average value
        virtual inline Real operator()() const { return average(); }

        /// Print average to output stream
        friend std::ostream& operator<<(std::ostream& os, const Average& avg) {
            os << avg();
            return os;
        }

    }; // Average

    // # Variance

    /**
     * \brief Calculates the variance of a sequence accumulatively
     * 
     * \tparam T: Numerical type 
     */
    class Variance: public Average
    {
    protected:

        Real sq_average_;  ///< Average of squared values

    public:

        /**
         * \brief Construct a new Variance object
         * 
         * \param zero: Zero of the given type
         */
        inline Variance()
        : Average(),
          sq_average_(0)
        {}

        /// Add value to Variance calculation
        virtual inline void operator()(const Real& new_value) override {
            Average::operator()(new_value);
            sq_average_ = (static_cast<Real>(this->count_)/(this->count_+1))*sq_average_ + (new_value*new_value)/(this->count_+1);
        }

        /**
         * \brief Add value to variance calculation
         * Usage: variance << new_value
         */
        inline Variance& operator<<(const Real& new_value) {
            this->operator()(new_value);
            return *this;
        }

        /// Clear variance calculation
        inline virtual void clear() override { Average::clear();    sq_average_ = 0; }

        /// Get Variance
        inline Real variance() const     { if (this->count_ <= 1) return -1; else return ((sq_average_ - this->average_*this->average_)*(this->count_/(this->count_-1.0))); }
        /// Get Standard Deviation
        inline Real stddev() const       { return sqrt(variance()); }

        /// Get Variance
        virtual inline Real operator()(void) const override { return variance(); }
    
        /// Print average to output stream
        friend std::ostream& operator<<(std::ostream& os, const Variance& var) {
            os << var.average() << " +- " << var.stddev();
            return os;
        }

    }; // Variance

    // # Least Squares

    /// Least square calculation on-the-fly
    class LeastSquares
    {
    public:

        using Container = std::vector<Real>;
        using Iter = Container::iterator;

    private:

        Container x_, y_;                   ///< Arrays for X an Y values
        Iter it_x_, it_y_;                  ///< Iterators on the containers
        Real Sx_, Sy_, Sx2_, Sy2_, Sxy_;    ///< Auxiliary values to store sums
        bool filled_;                       ///< Is the array filled

    private:
    
        inline void advance_() {
            it_x_++;
            it_y_++;
            if (it_x_ == x_.end()) {
                it_x_ = x_.begin();
                it_y_ = y_.begin();
                filled_ = true;
            }
        }

    public:

        /// Constructs with a given length
        inline LeastSquares(size_t length)
        : x_(length, 0), y_(length, 0),
          it_x_(x_.begin()), it_y_(y_.begin()),
          Sx_(0), Sy_(0), Sx2_(0), Sy2_(0), Sxy_(0),
          filled_(false)
        {}

        inline size_t size() const
        { if (filled_) return x_.size(); else return static_cast<size_t>(it_x_ - x_.begin()); }

        /// Add coordinates
        inline void operator()(const Real& x, const Real& y) {
            Sx_ += x - *it_x_;
            Sy_ += y - *it_y_;
            Sx2_ += SQ(x) - SQ(*it_y_);
            Sy2_ += SQ(y) - SQ(*it_y_);
            Sxy_ += x*y - (*it_x_)*(*it_y_);

            *it_x_ = x;
            *it_y_ = y;

            advance_();
        }

        /// Get average of X
        inline Real avgX() const { size_t N = size(); return (Sx_/N); }
        /// Get average of Y
        inline Real avgY() const { size_t N = size(); return (Sy_/N); }
        /// Get variance of X
        inline Real varX() const { size_t N = size(); return ((Sx2_/N) - SQ(Sx_/N)); }
        /// Get variance of Y
        inline Real varY() const { size_t N = size(); return ((Sy2_/N) - SQ(Sy_/N)); }

        /**
         * \brief Get \f$\alpha\f$ from \f$ x = \alpha y + \beta \f$
         * \return Real: alpha
         */
        inline Real alpha() { size_t N = size(); return((N*Sxy_ - Sx_*Sy_)/(N*Sx2_ - SQ(Sx_))); }
        /**
         * \brief Get \f$\beta\f$ from \f$ x = \alpha y + \beta \f$
         * \return Real: beta
         */
        inline Real beta()  { size_t N = size(); return((Sx2_*Sy_ - Sx_*Sxy_)/(N*Sx2_ - SQ(Sx_))); }

    }; // LeastSquares
    

    // # Histogram

    /// Fills a histogram vector for a given sequence
    class Histogram : public Accumulator<Real>
    {
    private:

        size_t n_bins_;             ///< Number of bins
        Real  bin_width_;           ///< Bin width
        std::vector<Real> bins_;    ///< Limits of the bins
        std::vector<size_t> hist_;  ///< Number of values on each bin

    public:

    /**
     * \brief Construct a new Histogram object in the \f$[\text{lower},\text{upper}]\f$ using \f$N_{\text{bins}}\f$ bins
     * 
     * \param lower: Lower bound
     * \param upper: Upper bound
     * \param n_bins: Number of bins
     */
    inline Histogram(Real lower, Real upper, size_t n_bins)
    : n_bins_(n_bins),
      bins_(n_bins+1), hist_(n_bins, 0)
    {
        ASSERT(upper > lower)
        bin_width_ = (upper - lower)/n_bins;
        for (size_t i = 0; i <= n_bins; i++) bins_[i] = lower + i* bin_width_;
    }

    /// Add value to histogram
    virtual void operator()(const Real& new_value) override;

    /// Add value to histogram
    inline Histogram& operator<<(const Real& new_value) {
        this->operator()(new_value);
        return *this;
    }

    /// Clear histogram
    virtual inline void clear() override {
        Accumulator<Real>::clear();
        hist_ = std::vector<size_t>(n_bins_, 0);
    }

    /// Access histogram bin value
    inline Real operator[](const size_t& i) const { ASSERT(i < n_bins_) return hist_[i]; }

    /// Get bins limits
    inline const std::vector<Real>& bins() const    { return bins_; }
    /// Get histogram values
    inline const std::vector<size_t>& hist() const  { return hist_; }

    /// Print histogram to output streams
    inline void print(std::ostream& output_stream) const {
        for (size_t i = 0; i < n_bins_; i++)
            output_stream << bins_[i] << "," << bins_[i+1] << "," << hist_[i] << std::endl;
    }

    // Plot
    inline void add_plot(Gnuplot& gplt) const {
        for (size_t i = 0; i < n_bins_; i++) {
            gplt.point2D((bins_[i]+bins_[i+1])/2, static_cast<Real>(hist_[i])/this->count());
        }
        gplt.endPoints();
    }

    inline void plot(Gnuplot& gplt) const {
        gplt("set style fill solid");
        gplt.beginPoints(1, "w boxes");
        add_plot(gplt);
        gplt.endPoints();
    }

    }; // Histogram

} // stats