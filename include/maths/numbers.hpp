#pragma once

/**
 * \file numbers.hpp
 * \author eliseuv (eliseuv816@gmail.com)
 * \brief Basic number definitions.
 * 
 */

#include "core.hpp"

// # Select floating point precision (float or double)

using Real = double;

// # Macro defined irrationals

// - Square roots
#define SQRT2       1.414213562373095048801688724209698079L ///< Square root of two \f$\sqrt{2}\f$
#define SQRT3       1.732050807568877293527446341505872367L ///< Square root of three \f$\sqrt{3}\f$
#define SQRT5       2.236067977499789696409173668731276235L ///< Square root of five \f$\sqrt{5}\f$
#define SQRT1_2     0.707106781186547524400844362104849039L ///< One over square root of two \f$\frac{1}{\sqrt{2}}\f$
#define SQRT1_3     0.577350269189625764509148780501957456L ///< One over square root of three \f$\frac{1}{\sqrt{3}}\f$
#define SQRT1_5     0.447213595499957939281834733746255247L ///< One over square root of three \f$\frac{1}{\sqrt{5}}\f$

// - Logarithms
#define LOG210      3.321928094887362347870319429489390176L ///< Logarithm base two of ten \f$log_{2}{10}\f$
#define LOG102      0.301029995663981195213738894724493027L ///< Logarithm base ten of two \f$log_{10}{2}\f$
#define EULER       2.718281828459045235360287471352662498L ///< Euler's constant \f$e\f$
#define LOG2E       1.442695040888963407359924681001892137L ///< Logarithm base two of Euler's constant \f$\log_{2}{e}\f$
#define LOG10E      0.434294481903251827651128918916605082L ///< Logarithm base ten of Euler's constant \f$\log_{10}{e}\f$
#define LN2         0.693147180559945309417232121458176568L ///< Natural logarithm of two \f$\ln{2}\f$
#define LN10        2.302585092994045684017991454684364208L ///< Natural logarith of ten \f$\ln{10}\f$

// - Pi
#define PI          3.141592653589793238462643383279502884L ///< Pi constant \f$\pi\f$
#define PI_2        1.570796326794896619231321691639751442L ///< A half of pi \f$\frac{\pi}{2}\f$
#define PI_3        1.047197551196597746154214461093167628L ///< A third of pi \f$\frac{\pi}{3}\f$
#define PI_4        0.785398163397448309615660845819875721L ///< A quarter of pi \f$\frac{\pi}{4}\f$
#define INV_PI      0.318309886183790671537767526745028724L ///< One over pi \f$\frac{1}{\pi}\f$
#define SQRTPI      1.772453850905516027298167483341145183L ///< Square root of pi \f$\sqrt{\pi}\f$
#define SQRT1_PI    0.564189583547756286948079451560772586L ///< One over square root of pi \f$\frac{1}{\sqrt{\pi}}\f$
#define RAD_DEG     0.017453292519943295769236907684886127L ///< Radians per degree \f$\frac{\pi}{180}\f$
#define DEG_RAD     57.29577951308232087679815481410517033L ///< Degrees per radian \f$\frac{180}{\pi}\f$