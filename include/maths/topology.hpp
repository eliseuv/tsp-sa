#pragma once

/**
 * \file topology.hpp
 * \author Eliseu Venites Filho (eliseuv816@gmail.com)
 * \brief Point set topology.
 *
 */

#include "core.hpp"
#include "maths/basic_math.hpp"
#include "maths/stochastic.hpp"
#include "tools/gnuplot.hpp"

namespace topo {

// # Finite Metric Space

template <typename PositiveReal = Real> class FiniteMetricSpace {
public:
  virtual size_t elementCount() const = 0;
  inline size_t pairCount() const {
    return ((elementCount() * (elementCount() - 1)) / 2);
  }

  virtual inline PositiveReal metric(size_t u, size_t v) const = 0;

  inline std::tuple<size_t, PositiveReal> nearestNeighbor(size_t i) const {
    PositiveReal metric_min = std::numeric_limits<PositiveReal>::max();
    size_t nn_index = i;
    for (size_t k = 0; k < i; k++) {
      PositiveReal metric_k = metric(i, k);
      if (metric_k < metric_min) {
        nn_index = k;
        metric_min = metric_k;
      }
    }
    for (size_t k = i + 1; k < elementCount(); k++) {
      PositiveReal metric_k = metric(i, k);
      if (metric_k < metric_min) {
        nn_index = k;
        metric_min = metric_k;
      }
    }
    return {nn_index, metric_min};
  }

  /// Metric List is a vector container for metric values
  using MetricList = Eigen::Matrix<PositiveReal, Eigen::Dynamic, 1>;

  /// Pairwise matric for all elements in subset
  inline MetricList pairwiseMetric() const {
    MetricList metric_list = MetricList::Zero(pairCount());
    typename MetricList::iterator it = metric_list.begin();
    for (size_t i = 0; i < elementCount(); i++) {
      for (size_t j = i + 1; j < elementCount(); j++) {
        *it = metric(i, j);
        it++;
      }
    }
    return metric_list;
  }

  using MetricMatrix = Eigen::Matrix<PositiveReal, Eigen::Dynamic,
                                     Eigen::Dynamic, Eigen::ColMajor>;

  /// Metric matrix for all elements in subset
  inline MetricMatrix metricMatrix() const {
    MetricMatrix metric_matrix =
        MetricMatrix::Zero(elementCount(), elementCount());
    for (size_t i = 0; i < elementCount(); i++) {
      for (size_t j = i + 1; j < elementCount(); j++) {
        metric_matrix(i, j) = metric(i, j);
        metric_matrix(j, i) = metric_matrix(i, j);
      }
    }
    return metric_matrix;
  }

}; // FiniteMetricSpace

// # Point Set Finite Metric Space

class PointSetFiniteMetricSpace : public FiniteMetricSpace<Real> {
public:
  using PointsMatrix =
      Eigen::Matrix<Real, Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor>;
  using Point = Eigen::Block<PointsMatrix, Eigen::Dynamic, 1, true>;
  using ConstPoint = Eigen::Block<const PointsMatrix, Eigen::Dynamic, 1, true>;

protected:
  PointsMatrix points_;

  inline PointSetFiniteMetricSpace(size_t dim, size_t node_count)
      : points_(PointsMatrix::Zero(dim, node_count)) {}

  inline PointSetFiniteMetricSpace(PointsMatrix &&points)
      : points_(std::move(points.transpose())) {}

public:
  inline const PointsMatrix &getPoints() const { return points_; }

  inline size_t dim() const { return points_.rows(); }
  virtual inline size_t elementCount() const final { return points_.cols(); }
  inline size_t pointCount() const { return elementCount(); }

  inline Point point(size_t u) {
    ASSERT(u < pointCount());
    return points_.col(u);
  }
  inline const ConstPoint point(size_t u) const {
    ASSERT(u < pointCount());
    return static_cast<ConstPoint>(points_.col(u));
  }

  inline Real &coord(size_t u, size_t i) {
    ASSERT((u < pointCount()) && (i < dim())) return points_(i, u);
  }
  inline const Real &coord(size_t u, size_t i) const {
    ASSERT((u < pointCount()) && (i < dim())) return points_(i, u);
  }

  void setCoordFromDist(size_t i, RealDistribution &dist, MT19937 &gen);
  void setAllCoordsFromDist(RealDistribution &dist, MT19937 &gen);

  void print(std::ostream &output_stream) const;

  void plot(Gnuplot &gplt, const std::string &opts = "pt 7") const;

}; // PointSetFiniteMetricSpace

// # Euclidean Space

class EuclideanSpace : public PointSetFiniteMetricSpace {
public:
  inline EuclideanSpace(size_t dim, size_t node_count)
      : PointSetFiniteMetricSpace(dim, node_count) {}

  inline EuclideanSpace(PointsMatrix &&points)
      : PointSetFiniteMetricSpace(std::move(points)) {}

  void distributeUniformly(MT19937 &gen);
  void distributeUniformly(MT19937 &gen, std::initializer_list<Real> size);
  void distributeRadially(MT19937 &gen, RealDistribution &radial_dist);

  void applyScale(Real scale);

  virtual inline Real metric(size_t u, size_t v) const final {
    return (point(u) - point(v)).norm();
  }

}; // EuclideanSpace

// # Euclidean space 2D

class EuclideanSpace2D : public EuclideanSpace {
public:
  inline EuclideanSpace2D(size_t node_count) : EuclideanSpace(2, node_count) {}

  inline EuclideanSpace2D(PointsMatrix &&points)
      : EuclideanSpace(std::move(points)) {
    ASSERT(dim() == 2)
  }

  void distributeUniformly(MT19937 &gen, Real size_x = 1, Real size_y = 1);
  void distributeRadially(MT19937 &gen, RealDistribution &radial_dist);
  void distributeCorrelatedCoords(MT19937 &gen, RealDistribution &dist,
                                  Real rho);

  Eigen::Matrix2d domainBounds() const;
  Real scaling() const;

}; // EuclideanSpace

// # Sphere Surface

class SphereSurface : public PointSetFiniteMetricSpace {
public:
  inline SphereSurface(size_t node_count)
      : PointSetFiniteMetricSpace(2, node_count) {}

  inline SphereSurface(PointsMatrix &&points)
      : PointSetFiniteMetricSpace(std::move(points)) {
    ASSERT(dim() == 2)
  }

  inline const Real &azimuthal(size_t u) const { return this->coord(u, 0); }
  inline const Real &polar(size_t u) const { return this->coord(u, 1); }

  void distributeUniformly(MT19937 &gen);
  void distributeUniformly(MT19937 &gen, Real theta);

  // - Projections onto 2D flat space
  EuclideanSpace2D plateCarree() const;
  EuclideanSpace2D mercator() const;

  virtual inline Real metric(size_t u, size_t v) const final {
    Real dAzimuthal = azimuthal(u) - azimuthal(v);
    Real dPolar = polar(u) - polar(v);
    return 2 * std::asin(std::sqrt(SQ(std::sin(dPolar / 2)) +
                                   std::sin(polar(u)) * std::sin(polar(v)) *
                                       SQ(std::sin(dAzimuthal / 2))));
  }

}; // SphereSurface

// # Cylinder Strip

class CylinderStrip : public PointSetFiniteMetricSpace {
protected:
  Real width_, height_;

public:
  inline CylinderStrip(size_t node_count, Real width = 1.0, Real height = 1.0)
      : PointSetFiniteMetricSpace(2, node_count), width_(width),
        height_(height) {}

  inline const Real &x(size_t u) const { return this->coord(u, 0); }
  inline const Real &y(size_t u) const { return this->coord(u, 1); }

  inline void distributeUniformly(MT19937 &gen) {
    UniformRealDistribution x_dist(0, width_);
    UniformRealDistribution y_dist(0, height_);
    this->setCoordFromDist(0, x_dist, gen);
    this->setCoordFromDist(1, y_dist, gen);
  }

  virtual inline Real metric(size_t u, size_t v) const override {
    Eigen::Vector2d du = this->point(v) - this->point(u);
    du[0] = ABS(du[0]);
    if (du[0] > (width_ / 2))
      du[0] = width_ - du[0];
    return du.norm();
  }

  EuclideanSpace2D unfold() const;

}; // CylinderStrip

// # Mobius Strip

class MobiusStrip : public CylinderStrip {
public:
  inline MobiusStrip(size_t node_count, Real width = 1.0, Real height = 1.0)
      : CylinderStrip(node_count, width, height) {}

  virtual inline Real metric(size_t u, size_t v) const final {
    Real dx = x(v) - x(u);
    // Check if u is closer to the image of v than to v itself
    if ((SQ(width_) + 4 * y(u) * y(v) - 2 * std::abs(dx) * width_) < 0) {
      Eigen::Vector2d point_v_image(x(v) - SIGN(dx) * width_, -y(v));
      return (point_v_image - point(u)).norm();
    } else
      return (point(v) - point(u)).norm();
  }

}; // MobiusStrip

} // namespace topo
