#pragma once

/**
 * \file tsallis.hpp
 * \author eliseuv (eliseuv816@gmail.com)
 * \brief Tsallis generalized statistics. Based on https://doi.org/10.1007/BF01016429
 * 
 */

#include "maths/basic_math.hpp"

namespace tsallis {

    /* Tsallis Entropy

        Generalized entropic form

            S_q(p) = k \frac{1 - \sum_i p_i^q}{q - 1}

        for q = 1

            S_1(p) = - k \sum_i p_i ln(p_i)

    */

    template <typename Real>
    Real entropy(const Real q, const std::vector<Real>& prob)
    {
        Real S = 0;
        if (q == 1) {
            for(auto p : prob)
                S -= p*std::log(p);
        }
        else
        {
            for(auto p : prob)
                S -= std::pow(p, q);
            S = (1+S)/(q-1);
        }
        return S;
    }

    /* Tsallis Canonical Ensemble

        Maximizing Tsallis entropy with a total energy constraint

            \sum_i p_i^q

        yields

            p_i \propto (1 - \beta(1-q)E_i)^\frac{1}{1-q}

        for q = 1, Boltzmann-Gibbs statistics is recovered

            p_i \propto exp(-\beta E_i)

    */
    template <typename Real>
    Real ensemble(const Real q, const Real beta, const Real E)
    {
    if (q == 1.0) return std::exp(-beta*E);
    if ((q < 1.0) && (beta*(1-q)*E > 1)) return 0;
    return std::pow(1-beta*(1-q)*E, 1.0/(1.0-q));
    }

    template <typename Real>
    Real ensembleRatio(const Real q, const Real T, const Real E, const Real dE)
    {
    if (q == 1)
        return std::exp(-dE/T);
    else
        return std::pow(1 + dE/(T/(q-1) + E), 1.0/(1.0-q));
    }

    /* Tsallis Visiting function

        From the original paper: https://doi.org/10.1007/BF01016429
    
    */
    template <typename Real>
    Real visiting(const Real q, const Real T, const Real gasdev_x, const Real gasdev_y)
    {
    Real fator1 = std::exp(std::log(T)/(q-1.0));
    Real fator2 = std::exp((4.0-q)*std::log(q-1.0));
    Real fator3 = std::exp((2.0-q)*std::log(2.0)/(q-1.0));
    Real fator4 = (SQRTPI*fator1*fator2)/(fator3*(3.0-q));
    Real fator5 = (1.0/(q-1.0))-0.5;
    Real fator6 = PI*(1.0-fator5)/std::sin(PI*(1.0-fator5))/std::exp(std::lgamma(2.0-fator5));

    Real sigmax = std::exp(-(q-1.0)*std::log(fator6/fator4)/(3.0-q));
    Real x = sigmax*gasdev_x;
    Real y = gasdev_y;
    Real den = std::exp((q-1.0)*std::log(std::abs(y))/(3.0-q));

    return x/den;

    }

    /* Tsallis Cooling Schedule

        In order to guarantee a global minimum at t -> \infty

            T_{q_v}(t) = T_{q_v}(1) \frac{2^(q_v-1) - 1}{(1+t)^(q_v-1) - 1}

        for q_v = 1

            T_1(t) = T_1(1) \frac{ln 2}{ln(t+1)}

        for q_v = 2

            T_2(t) = T_2(1)/t

    */
    template <typename Real>
    Real cooling(const Real q_v, const size_t t)
    {
        if (q_v == 1)
            return std::log((Real)2.0)/std::log(t+1.0);

        return (std::pow(2.0, q_v-1.0) - 1.0)/(std::pow(t+1.0, q_v-1.0) - 1.0);

    }

} // tsallis