#pragma once

/**
 * \file markov.hpp
 * \author eliseuv (eliseuv816@gmail.com)
 * \brief Markov Chain methods.
 * 
 */

#include "core.hpp"
#include "maths/numbers.hpp"
#include "maths/stochastic.hpp"

// # Markov Chain

/**
 * \brief Interface providing Markov Chain methods
 * 
 * \tparam State=size_t: State of the Markov Chain
 */
template <typename State=size_t>
class MarkovChain
{
public:

    /// Get current state
    virtual const State& getCurrentState() const = 0;
    /// Set current state
    virtual void setCurrentState(const State& state) = 0;

    /// Select random state to go next
    virtual void selectNextState(MT19937& gen) = 0;
    /// Jump to selected state
    virtual void jumpNextState() = 0;

}; // MarkovChain

// # Markov Chain Monte Carlo

/**
 * \brief MCMC has a numerical value associated with each state of the Markov Chain and a way of calculating local differences of this value.
 *
 * \tparam State=size_t: State of the Markov Chain
 * \tparam Num=Real: Numerical value associated with the state
 */
template <typename State=size_t, typename Num=Real>
class MCMC: public MarkovChain<State>
{
public:

    /// Get value associated with current state
    virtual Num getCurrentValue() const = 0;

    /// Get value difference between current state and selected state
    virtual Num getLocalValue() const = 0;

    /// Calculate value associated with the current state   // ! COSTLY
    virtual void calculateCurrentValue() = 0;
    /// Update global value using local value
    virtual void updateValueThenJumpNextState() = 0;
    
};

//=================================================================================================

// - Examples

// * Example of Markov Chain: 1D Disctrete Random Walk
/**
 * \brief Example implementation of a 1D Random Walk usign Markov Chain class
 * 
 */
class RandomWalk1D: public MarkovChain<int>
{
private:
    /// Pointer to variable holding position
    int* position_;
    /// Selected state of process (i.e. will go left or right)
    int dposition_; // = -1, +1
    /// Coin toss
    std::bernoulli_distribution coin_toss_;

public:

    /// Constructor a given initial position
    inline RandomWalk1D(int& position)
      : position_(&position),
        dposition_(0),
        coin_toss_()
    {}

    /// Get current state
    virtual inline const int& getCurrentState() const final { return *position_; }

    /// Set state
    virtual inline void setCurrentState(const int& position) final { *position_ = position; }

    /// Coin toss to select next state
    virtual inline void selectNextState(MT19937& gen) final { (coin_toss_(gen)) ? dposition_++ : dposition_--; }

    /// Change to state selected
    virtual inline void jumpNextState() final { *position_ += dposition_; }

}; // RandomWalk1D