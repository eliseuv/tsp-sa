#pragma once

/**
 * \file annealer.hpp
 * \author eliseuv (eliseuv816@gmail.com)
 * \brief Annealer class for Simulated Annealing
 * 
 */

#include "tools/file_io.hpp"
#include "sampling/metropolis.hpp"

// # Annealer

/**
 * \brief Annealer class for Simulated Annealing
 *
 * \tparam State=size_t: State of the associated MCMC
 */
class Annealer: public Metropolis
{
public:

    /**
     * \brief Cooling Schedule
     * Determines the next value of beta based on initial beta and step number.
     * \f$ \beta_{t} = beta_{t}(\beta_0, t) \f$
     */
    using CoolingSched = std::function<Real(Real, size_t)>;

public:

    // - Cooling Schedules

    static CoolingSched geometricCooling(Real alpha)
    { return [alpha](Real initial_beta, size_t t)->Real { return std::pow(alpha, t)*initial_beta; }; }

    static size_t geometricCoolingStepCount(Real initial_beta, Real final_beta, Real alpha)
    { return std::ceil(std::log(final_beta/initial_beta)/std::log(alpha)); }

    static CoolingSched tsallisCooling(Real q_v)
    { return [q_v]([[maybe_unused]] Real initial_beta, size_t t)->Real { return initial_beta/tsallis::cooling(q_v, t); }; }

protected:

    // Cooling Schedule
    CoolingSched cooling_;

public:

    // - Constructor

    inline Annealer(Real beta, CoolingSched cooling=geometricCooling(1.001001001001001001), typename Metropolis::Ensemble ensemble=Metropolis::gibbsEnsemble())
    : Metropolis(beta, ensemble),
      cooling_(cooling)
    {}

    // Temperature
    inline Real temperature() const {
        if (this->beta_ <= 0) return -1; // T -> inf
        else return (1.0/this->beta_);
    }

    // - Cooling Schedule
    // Set cooling schedule
    inline void setCoolingSched(const CoolingSched& cooling)                 { cooling_ = cooling; }

    // Find initial beta
    template <typename State>
    inline Real findInitialBeta(MCMC<State, Real>& mcmc, size_t n_reject, size_t n_accept, Real accepted_ratio=0.95, Real beta_multiplier=0.9);

    // Annealing until a final beta is reached
    template <typename State>
    inline size_t annealToBeta(MCMC<State, Real>& mcmc, size_t n_samples, Real final_beta);
    template <typename State>
    inline size_t annealToBeta(MCMC<State, Real>& mcmc, size_t n_samples, Real final_beta, std::ostream& output_stream);
    
    template <typename State>
    inline void annealFixedSteps(MCMC<State, Real>& mcmc, size_t n_samples, size_t n_steps);
    template <typename State>
    inline void annealFixedSteps(MCMC<State, Real>& mcmc, size_t n_samples, size_t n_steps, std::ostream& output_stream);

    /*
    template <typename State>
    inline size_t operator()(MCMC<State, Real>& mcmc, size_t n_reject, size_t n_accept);
    template <typename State>
    inline size_t operator()(MCMC<State, Real>& mcmc, size_t n_reject, size_t n_accept, std::ostream& output_stream);

    template <typename State>
    inline void annealToBetaOpt(MCMC<State, Real>& mcmc, size_t n_samples, Real final_beta, std::ostream& output_stream);
    

    // Old Bachelor Acceptance
    template <typename State>
    inline size_t OBA(MCMC<State, Real>& mcmc, size_t n_reject, size_t n_accept, std::ostream& output_stream);
    */
}; // Annealer