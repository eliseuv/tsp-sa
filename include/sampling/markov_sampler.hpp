#pragma once

/**
 * \file sampler.hpp
 * \author eliseuv (eliseuv816@gmail.com)
 * \brief Simple sampler class.
 * 
 */

#include "sampling/markov.hpp"
#include "maths/statistics.hpp"

// TODO: Make sampling methods be template on MCMC::State instead of the whole class.

/**
 * TODO: Make sampling methods be template on MCMC::State instead of the whole class.
 * This may hinder sampling methods from being virtual.
 * On the other hand the same Sampler class may be used for MCMC with different State.
 */

// # Sampler

/**
 * \brief Sample a Markov Chain
 * 
 * \tparam State=size_t: State of the associated MCMC
 * \tparam Num=Real: Numerical value associated with each state of the MCMC
 */
class MarkovSampler
{
protected:

    /// PRNG 
    MT19937 gen_;

public:

    inline MarkovSampler()
    : gen_(randomSeed())
    {}

    template <typename State, typename Num>
    inline void sample(MCMC<State, Num>& mcmc, size_t n_samples);                                        ///< Sample with no output
    template <typename State, typename Num>
    inline void sample(MCMC<State, Num>& mcmc, size_t n_samples, stats::Accumulator<Num>& accumulator);  ///< Sample to stats::Accumulator
    template <typename State, typename Num>
    inline void sample(MCMC<State, Num>& mcmc, size_t n_samples, std::ostream& output_stream);           ///< Sample to output stream
    
}; // Sampler