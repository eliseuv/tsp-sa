#pragma once

#include "sampling/annealer.hpp"

#include "sampling/metropolis.tpp"

// # Annealer

// - Find initial temperature

template <typename State>
inline Real Annealer::findInitialBeta(MCMC<State>& mcmc, size_t n_reject, size_t n_accept, Real accepted_ratio, Real beta_multiplier) {
    while(1) {
        size_t rejected, accepted;
        std::tie(rejected, accepted) = this->sampleUntil_(mcmc, n_reject, n_accept);
        if (accepted >= accepted_ratio*(accepted+rejected)) return this->beta_;
        else this->beta_ *= beta_multiplier;
    }
}

// - Boltzmann Machine

template <typename State>
inline size_t Annealer::annealToBeta(MCMC<State>& mcmc, size_t n_samples, Real final_beta) {
    size_t total_samples = 0;
    // Annealing loop
    Real initial_beta = this->beta_;
    size_t t = 0;
    while(this->beta_ < final_beta) {
        // Metropolis
        this->sample_(mcmc, n_samples);
        total_samples += n_samples;
        // Update temperature
        t++;
        this->beta_ = this->cooling_(initial_beta, t);
    } // Annealing loop

    mcmc.calculateCurrentValue();

    return total_samples;
}

template <typename State>
inline size_t Annealer::annealToBeta(MCMC<State>& mcmc, size_t n_samples, Real final_beta, std::ostream& output_stream) {
    
    size_t total_samples = 0;

    // Annealing loop
    Real initial_beta = this->beta_;
    size_t t = 0;
    while(this->beta_ < final_beta) {
        // Metropolis
        this->sample_(mcmc, n_samples);
        mcmc.calculateCurrentValue();
        output_stream << this->beta_ << "," << mcmc.getCurrentValue() << std::endl;
        total_samples += n_samples;

        // Update temperature
        t++;
        this->beta_ = this->cooling_(initial_beta, t);

    } // Annealing loop

    return total_samples;
}

template <typename State>
inline void Annealer::annealFixedSteps(MCMC<State>& mcmc, size_t n_samples, size_t n_steps)
{   
    // Annealing loop
    Real initial_beta = this->beta_;

    for (size_t t = 1; t <= n_steps; t++) {
        // Metropolis
        this->sample_(mcmc, n_samples);
        // Update temperature
        this->beta_ = this->cooling_(initial_beta, t);
    } // Annealing loop

    // Revert to last sampled temperature
    this->beta_ = this->cooling_(initial_beta, n_steps-1);

    mcmc.calculateCurrentValue();
}

template <typename State>
inline void Annealer::annealFixedSteps(MCMC<State>& mcmc, size_t n_samples, size_t n_steps, std::ostream& output_stream)
{   
    // Annealing loop
    Real initial_beta = this->beta_;

    for (size_t t = 1; t <= n_steps; t++) {
        // Metropolis
        this->sample(mcmc, n_samples);
        output_stream << this->beta_ << '\t' << mcmc.getCurrentValue() << std::endl;
        // Update temperature
        this->beta_ = this->cooling_(initial_beta, t);
    } // Annealing loop

    // Revert to last sampled temperature
    this->beta_ = this->cooling_(initial_beta, n_steps-1);

    mcmc.calculateCurrentValue();
}

/*
template <typename State>
inline size_t Annealer::operator()(MCMC<State>& mcmc, size_t n_reject, size_t n_accept)
{
    size_t total_samples = 0;

    // Annealing loop
    size_t t = 0;
    while (1) {

        size_t rejected, accepted;
        std::tie(rejected, accepted) = this->sampleUntil_(mcmc, n_reject, n_accept);
        total_samples += rejected + accepted;

        // If no moves were accepted
        if (accepted == 0) break;
        t++;
        this->beta_ = this->cooling_(initial_beta, t);  
    } // Annealing loop

    mcmc.calculateCurrentValue();
    return total_samples;
}

template <typename State>
inline size_t Annealer::operator()(MCMC<State>& mcmc, size_t n_reject, size_t n_accept, std::ostream& output_stream)
{
    size_t total_samples = 0;

    // Annealing loop
    size_t temperature_step = 0;
    while (1) {
        temperature_step++;

        size_t rejected, accepted;
        std::tie(rejected, accepted) = this->sampleUntil_(mcmc, n_reject, n_accept);
        total_samples += rejected + accepted;

        // Update Value
        mcmc.calculateCurrentValue();
        output_stream << this->beta_ << "," << rejected << "," << accepted << "," << mcmc.getCurrentValue() << std::endl;

        // If no moves were accepted
        if (accepted == 0) break;
        
        applyCooling(temperature_step);
    } // Annealing loop

    return total_samples;
}

template <typename State>
inline void Annealer::annealToBetaOpt(MCMC<State>& mcmc, size_t n_samples, Real final_beta, std::ostream& output_stream) {
    
    State state_min = mcmc.getCurrentState();
    Real value_min = mcmc.getCurrentValue();

    // Annealing loop
    size_t temperature_step = 0;
    while(this->beta_ < final_beta) {
        temperature_step++;

        // Metropolis
        for (size_t n = 0; n < n_samples; n++) {
            mcmc.selectNextState(this->gen_);
            // Metropolis
            Real denergy = mcmc.getLocalValue();
            if (denergy > 0) { // Energy increses => Metropolis test
                Real metropolis = met_dist_(met_gen_);
                if (metropolis < ensemble_(beta_, denergy)) { // Accept
                    mcmc.addLocalValue();
                    mcmc.jumpNextState();
                    if (mcmc.getCurrentValue() < value_min) {
                        mcmc.calculateCurrentValue();
                        state_min = mcmc.getCurrentState();
                        value_min = mcmc.getCurrentValue();
                    }
                }
            }
            else { // Energy decreases => Accept
                mcmc.addLocalValue();
                mcmc.jumpNextState();
                if (mcmc.getCurrentValue() < value_min) {
                    mcmc.calculateCurrentValue();
                    state_min = mcmc.getCurrentState();
                    value_min = mcmc.getCurrentValue();
                }
            } // Metropolis
        }

        output_stream << this->beta_ << "," << mcmc.getCurrentValue() << std::endl;

        // Update temperature
        applyCooling(temperature_step);

    } // Annealing loop

    mcmc.setCurrentState(state_min);
}

// - OBA Machine

template <typename State>
inline size_t Annealer::OBA(MCMC<State>& mcmc, size_t n_reject, size_t n_accept, std::ostream& output_stream)
{
    // Tsallis statistics
    Real q_a = 1.0;
    const Real q_min = -5.0, q_max = 1.0, dq = 1.0;
    this->ensemble_ = this->tsallisEnsemble(q_a);

    size_t total_samples = 0;

    // Annealing loop
    size_t temperature_step = 1;
    while (1) {
        const Real prev_value = mcmc.value();

        size_t rejected, accepted;
        std::tie(rejected, accepted) = this->sampleUntil_(mcmc, n_reject, n_accept);
        total_samples += rejected + accepted;

        // Update Value
        mcmc.calculateCurrentValue();
        output_stream << this->beta_ << "," << q_a << "," << mcmc.value() << std::endl;

        // Update parameters
        if ((mcmc.value() < prev_value) && (q_a > q_min)) { // If new cost is lower
            // Decrease q_a until q_min
            q_a -= dq;
            this->ensemble_ = this->tsallisEnsemble(q_a);
        }
        else { // If new cost is equal or higher
            // Increase q_a until q_max
            if (q_a < q_max) {
                q_a += dq;
                this->ensemble_ = this->tsallisEnsemble(q_a);
            }
            else { // If q_a hits q_max

                // If no moves were accepted
                if (accepted == 0) break;

                // Reset q_a to 1 and cool system
                q_a = 1.0;
                this->ensemble_ = this->tsallisEnsemble(q_a);
                applyCooling(temperature_step);
                temperature_step++;
                std::cout << this->beta_ << "\t" << mcmc.getCurrentValue() << std::endl;
            }
        }
    } // Annealing loop

    // Update Value
    mcmc.calculateCurrentValue();

    return total_samples;
}
*/