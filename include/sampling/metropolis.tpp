#pragma once

#include "sampling/metropolis.hpp"
#include "maths/statistics.hpp"

#include "sampling/markov_sampler.tpp"

// # Metropolis Sampler

// - beta -> inf (T -> 0)

// Sample beta -> inf a fixed number of samples
template <typename State>
inline void Metropolis::sampleInfiniteBeta(MCMC<State>& mcmc, size_t n_samples) {
    for (size_t n = 0; n < n_samples; n++) {
        mcmc.selectNextState(this->gen_);
        if (mcmc.getLocalValue() < 0) // Energy decreases => Accept
            mcmc.jumpNextState();
    }
    mcmc.calculateCurrentValue();
}

// Sample beta -> inf a fixed number of samples
template <typename State>
inline void Metropolis::sampleInfiniteBeta(MCMC<State>& mcmc, size_t n_samples, stats::Accumulator<Real>& accumulator) {
    for (size_t n = 0; n < n_samples; n++) {
        mcmc.selectNextState(this->gen_);
        if (mcmc.getLocalValue() < 0) // Energy decreases => Accept
            mcmc.updateValueThenJumpNextState();
        accumulator(mcmc.value());
    }
}

// Sample beta -> inf a fixed number of samples
template <typename State>
inline void Metropolis::sampleInfiniteBeta(MCMC<State>& mcmc, size_t n_samples, std::ostream& output_stream) {
    // Monte Carlo loop
    for (size_t n = 0; n < n_samples; n++) {
        mcmc.selectNextState(this->gen_);
        if (mcmc.getLocalValue() < 0) // Energy decreases => Accept
            mcmc.updateValueThenJumpNextState();
        output_stream << mcmc.getCurrentValue() << '\n';
    }
}

// Sample beta -> inf with a stopping criterium
template <typename State>
inline std::tuple<size_t, size_t> Metropolis::sampleInfiniteBetaUntil(MCMC<State>& mcmc, size_t n_reject, size_t n_accept) {
    size_t rejected = 0;
    size_t accepted = 0;

    // Sampling loop
    while(1) {
        mcmc.selectNextState(this->gen_);
        // Metropolis
        if (mcmc.getLocalValue() < 0) { // Energy decreases => Accept
            mcmc.jumpNextState();
            accepted++;
            if (accepted == n_accept) break;
        }
        else { // Energy increases => Reject
            rejected++;
            if (rejected == n_reject) break;
        } // Metropolis
    } // Sampling loop

    mcmc.calculateCurrentValue();
    return std::make_tuple(rejected, accepted);
}

// - Finite beta

// Sample a fixed number of samples
template <typename State>
inline void Metropolis::sample_(MCMC<State>& mcmc, size_t n_samples)
{
    // Sampling loop
    for (size_t n = 0; n < n_samples; n++) {
        mcmc.selectNextState(this->gen_);
        // Metropolis
        Real denergy = mcmc.getLocalValue();
        if (denergy > 0) { // Energy increses => Metropolis test
            if (uniformRandomValue_() < ensemble_(beta_, denergy)) // Accept
                mcmc.jumpNextState();
        }
        else // Energy decreases => Accept
            mcmc.jumpNextState();
    }
}

// Sample with a stopping criterium
template <typename State>
inline std::tuple<size_t, size_t> Metropolis::sampleUntil_(MCMC<State>& mcmc, size_t n_reject, size_t n_accept)
{
    size_t rejected = 0;
    size_t accepted = 0;

    // Sampling loop
    while(1) {
        mcmc.selectNextState(this->gen_);
        // Metropolis
        Real denergy = mcmc.getLocalValue();
        if (denergy > 0) { // Energy increses => Metropolis test
            if (uniformRandomValue_() < ensemble_(beta_, denergy)) { // Accept
                mcmc.jumpNextState();
                accepted++;
                if (accepted == n_accept) break;
            }
            else {
                rejected++;
                if (rejected == n_reject) break;
            }
        }
        else { // Energy decreases => Accept
            mcmc.jumpNextState();
            accepted++;
        } // Metropolis
    } // Sampling loop

    return std::make_tuple(rejected, accepted);
}

// Metropolis step
template <typename State>
inline bool Metropolis::step(MCMC<State>& mcmc)
{
    mcmc.selectNextState(this->gen_);
    // Metropolis
    Real denergy = mcmc.getLocalValue();
    if (denergy > 0) { // Energy increses => Metropolis test
        if (uniformRandomValue_() < ensemble_(beta_, denergy)) { // Accept
            mcmc.updateValueThenJumpNextState();
            return true;
        }
    }
    else { // Energy decreases => Accept
        mcmc.updateValueThenJumpNextState();
        return true;
    } // Metropolis
    return false;
}

// Sample a fixed number of samples
template <typename State>
inline void Metropolis::sample(MCMC<State>& mcmc, size_t n_samples)
{
    sample_(mcmc, n_samples);
    mcmc.calculateCurrentValue();
}

// Sample a fixed number of samples
template <typename State>
inline void Metropolis::sample(MCMC<State>& mcmc, size_t n_samples, stats::Accumulator<Real>& accumulator)
{
    for (size_t n = 0; n < n_samples; n++) {
        mcmc.selectNextState(this->gen_);
        // Metropolis
        Real denergy = mcmc.getLocalValue();
        if (denergy > 0) { // Energy increses => Metropolis test
            if (uniformRandomValue_() < ensemble_(beta_, denergy)) { // Accept
                mcmc.updateValueThenJumpNextState();
            }
        }
        else { // Energy decreases => Accept
            mcmc.updateValueThenJumpNextState();
        } // Metropolis
        accumulator(mcmc.getCurrentValue());
    }
}

// Sample a fixed number of samples
template <typename State>
inline void Metropolis::sample(MCMC<State>& mcmc, size_t n_samples, std::ostream& output_stream)
{
    for (size_t n = 0; n < n_samples; n++) {
        mcmc.selectNextState(this->gen_);
        // Metropolis
        Real denergy = mcmc.getLocalValue();
        if (denergy > 0) { // Energy increses => Metropolis test
            if (uniformRandomValue_() < ensemble_(beta_, denergy)) { // Accept
                mcmc.updateValueThenJumpNextState();
            }
        }
        else { // Energy decreases => Accept
            mcmc.updateValueThenJumpNextState();
        } // Metropolis
        output_stream << mcmc.getCurrentValue() << '\n';
    }
}

// Statistics on Samples
template <typename State>
inline std::pair<Real, Real> Metropolis::energyStats(MCMC<State, Real>& mcmc, size_t n_samples)
{
    std::vector<Real> energy(n_samples);

    for (size_t n = 0; n < n_samples; n++) {
        mcmc.selectNextState(this->gen_);
        // Metropolis
        Real denergy = mcmc.getLocalValue();
        if (denergy > 0) { // Energy increses => Metropolis test
            if (uniformRandomValue_() < ensemble_(beta_, denergy)) { // Accept
                mcmc.updateValueThenJumpNextState();
            }
        }
        else { // Energy decreases => Accept
            mcmc.updateValueThenJumpNextState();
        } // Metropolis
        energy[n] = mcmc.getCurrentValue();
    }

    return stats::vectorAverageVariance(energy);
}

// Sample a fixed number of samples
template <typename State>
inline std::tuple<size_t, size_t> Metropolis::sampleCount(MCMC<State>& mcmc, size_t n_samples)
{
    size_t rejected = 0;
    size_t accepted = 0;

    // Sampling loop
    for (size_t n = 0; n < n_samples; n++){
        mcmc.selectNextState(this->gen_);
        // Metropolis
        Real denergy = mcmc.getLocalValue();
        if (denergy > 0) { // Energy increses => Metropolis test
            if (uniformRandomValue_() < ensemble_(beta_, denergy)) { // Accept
                mcmc.jumpNextState();
                accepted++;
            }
            else {
                rejected++;
            }
        }
        else { // Energy decreases => Accept
            mcmc.jumpNextState();
            accepted++;
        } // Metropolis
    } // Sampling loop

    mcmc.calculateValue();
    return std::make_tuple(rejected, accepted);
}

// Sample with a stopping criterium
template <typename State>
inline std::tuple<size_t, size_t> Metropolis::sampleUntil(MCMC<State>& mcmc, size_t n_reject, size_t n_accept)
{
    auto steps_pair = sampleUntil_(mcmc, n_reject, n_accept);
    mcmc.calculateCurrentValue();
    return steps_pair;
}