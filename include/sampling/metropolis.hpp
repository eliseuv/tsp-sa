#pragma once

// # Metropolis Sampler

/**
 * \file metropolis.hpp
 * \author eliseuv (eliseuv816@gmail.com)
 * \brief Metropolis Sampler.
 * 
 */

#include "maths/tsallis.hpp"
#include "sampling/markov.hpp"
#include "sampling/markov_sampler.hpp"

/**
 * \brief Samples a system at a given \f$\beta\f$ using the Metropolis-Hastings algorithm
 */
class Metropolis : protected MarkovSampler
{
public:

    using Ensemble = std::function<Real(Real, Real)>;   ///< Canonical Emsemble \f$p_i = p_i(\beta,E_i)\f$

protected:

    /// Metropolis PRNG
    MT19937 met_gen_;
    /// Uniform distribution on \f$[0,1]\f$
    UniformDistribution<Real> met_dist_;

    /// Beta (\f$\beta = 1/T\f$)
    Real beta_;

    /// Ensemble used
    Ensemble ensemble_;

    inline Real uniformRandomValue_() { return met_dist_(met_gen_); }

public:

    // - Ensembles

    /// Gibbs Canonical Ensemble
    /** \f$p_i \propto \exp(-\beta E_i)\f$ */
    static Ensemble gibbsEnsemble()
    { return [](Real beta, Real denergy)->Real{ return std::exp(-beta*denergy); }; }

    /// Tsallis Canonical Ensemble
    /** \f$p_i(q_a) \propto (1 - \beta(1-q)E_i)^\frac{1}{1-q}\f$ */
    static Ensemble tsallisEnsemble(const Real& q_a)
    { return [q_a](Real beta, Real denergy)->Real { return tsallis::ensemble(q_a, beta, denergy); }; }

    // - Constructor

    inline Metropolis(Real beta, Ensemble ensemble=gibbsEnsemble())
    : MarkovSampler(),
      met_gen_(randomSeed()), met_dist_(0, 1),
      beta_(beta), ensemble_(ensemble)
    {}

    // - Members

    // Beta parameter
    inline const Real& beta() const  { return beta_; }
    inline void beta(Real beta)      { ASSERT(beta >= 0) beta_ = beta; }

    // Ensemble
    inline Real ensemble(Real beta, Real denergy) const  { return ensemble_(beta, denergy); }
    inline void ensemble(Ensemble ensemble)                 { ensemble_ = ensemble; }

    // Assignment operator
    Metropolis& operator=(const Metropolis& other) = default;

    // - beta -> 0      (T -> inf)

    // Fixed number of steps
    template <typename State>
    inline void sampleZeroBeta(MCMC<State, Real>& mcmc, size_t n_samples)                                         { MarkovSampler::sample(mcmc, n_samples); }
    template <typename State>
    inline void sampleZeroBeta(MCMC<State, Real>& mcmc, size_t n_samples, stats::Accumulator<Real>& accumulator)  { MarkovSampler::sample(mcmc, n_samples, accumulator); }
    template <typename State>
    inline void sampleZeroBeta(MCMC<State, Real>& mcmc, size_t n_samples, std::ostream& output_stream)            { MarkovSampler::sample(mcmc, n_samples, output_stream); }

    // - beta -> inf    (T -> 0)

    // Fixed number of steps
    template <typename State>
    inline void sampleInfiniteBeta(MCMC<State, Real>& mcmc, size_t n_samples);
    template <typename State>
    inline void sampleInfiniteBeta(MCMC<State, Real>& mcmc, size_t n_samples, stats::Accumulator<Real>& accumulator);
    template <typename State>
    inline void sampleInfiniteBeta(MCMC<State, Real>& mcmc, size_t n_samples, std::ostream& output_stream);
    // Sample until stopping criterium
    template <typename State>
    inline std::tuple<size_t, size_t> sampleInfiniteBetaUntil(MCMC<State, Real>& mcmc, size_t n_reject, size_t n_accept=0);

    // - finite beta

protected:

    template <typename State>
    inline void sample_(MCMC<State, Real>& mcmc, size_t n_samples);
    template <typename State>
    inline std::tuple<size_t, size_t> sampleUntil_(MCMC<State, Real>& mcmc, size_t n_reject, size_t n_accept=0);

public:

    // Single step
    template <typename State>
    inline bool step(MCMC<State, Real>& mcmc);
    // Fixed number of steps
    template <typename State>
    inline void sample(MCMC<State, Real>& mcmc, size_t n_samples);
    template <typename State>
    inline void sample(MCMC<State, Real>& mcmc, size_t n_samples, stats::Accumulator<Real>& accumulator);
    template <typename State>
    inline void sample(MCMC<State, Real>& mcmc, size_t n_samples, std::ostream& output_stream);

    // Statistics on Samples
    template <typename State>
    inline std::pair<Real, Real> energyStats(MCMC<State, Real>& mcmc, size_t n_samples);

    template <typename State>
    inline std::tuple<size_t, size_t> sampleCount(MCMC<State, Real>& mcmc, size_t n_samples);
    // Sample until stopping criterium
    template <typename State>
    inline std::tuple<size_t, size_t> sampleUntil(MCMC<State, Real>& mcmc, size_t n_reject, size_t n_accept=0);

}; // Metropolis