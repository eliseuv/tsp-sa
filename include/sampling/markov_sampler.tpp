#pragma once

#include "sampling/markov_sampler.hpp"

// # Sampler

// - Sampling

// Inlined, no output
template <typename State, typename Num>
inline void MarkovSampler::sample(MCMC<State, Num>& mcmc, size_t n_samples)
{
  for (size_t n = 0; n < n_samples; n++) {
    mcmc.selectNextState(gen_);
    mcmc.jumpNextState();
  }
  mcmc.calculateCurrentValue();
}

// To stats::Accumulator
template <typename State, typename Num>
inline void MarkovSampler::sample(MCMC<State, Num>& mcmc, size_t n_samples, stats::Accumulator<Num>& accumulator)
{
  for (size_t n = 0; n < n_samples; n++) {
    mcmc.selectNextState(gen_);
    mcmc.updateValueThenJumpNextState();
    accumulator(mcmc.getCurrentValue());
  }
}

// To output stream
template <typename State, typename Num>
inline void MarkovSampler::sample(MCMC<State, Num>& mcmc, size_t n_samples, std::ostream& output_stream)
{
  for (size_t n = 0; n < n_samples; n++) {
    mcmc.selectNextState(gen_);
    mcmc.updateValueThenJumpNextState();
    output_stream << mcmc.getCurrentValue() << "\n";
  }
}