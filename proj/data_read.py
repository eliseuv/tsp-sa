import os
import re
import numpy as np
import pandas as pd

# # Returns list of datafiles in a given dir

def get_datafiles_list(data_dir, reqs_dict={}, extension='.csv'):
    datafiles_list = []
    for filename in os.listdir(data_dir):
        # Ignore non data files
        _, ext = os.path.splitext(filename)
        if ext != extension:
            continue
        datafiles_list.append(os.path.join(data_dir,filename))
    return datafiles_list

# # Filters a list of datafiles for only those that meet certain requirements

def filter_datafiles_list(datafiles_list, reqs_dict):
    filtered_datafiles_list = []
    for datafile in datafiles_list:
        filename = os.path.basename(datafile)
        req_check = True
        for req_name, req_value in reqs_dict.items():
            value = re.findall(req_name+'=(.+?),', filename)
            if (value == []) or (str(value[0]) != str(req_value)):
                req_check = False
                break
        if not req_check:
            continue
        filtered_datafiles_list.append(datafile)
    return filtered_datafiles_list

# # Organizes a datafiles in a dict given a key

def datafiles_dict_add_key(datafiles, key_name, key_type):
    if isinstance(datafiles, list):
        result = {}
        for datafile in datafiles:
            filename = os.path.basename(datafile)
            key_val = re.findall(key_name+'=(.+?),', filename)
            if key_val == []:
                continue
            key_val = key_type(key_val[0])
            if key_val not in result.keys():
                result[key_val] = []
            result[key_val].append(datafile)
        return result
    elif isinstance(datafiles, dict):
        for key in datafiles.keys():
            datafiles[key] = datafiles_dict_add_key(datafiles[key], key_name, key_type)
        return datafiles

def datafiles_dict(datafiles, keys_dict):
    for key_name, key_type in keys_dict.items():
        datafiles = datafiles_dict_add_key(datafiles, key_name, key_type)
    return datafiles

# # Make each dict entry correspond to only one datafile

def make_unique_dict(datafiles):
    if isinstance(datafiles, list):
        return datafiles[0]
    elif isinstance(datafiles, dict):
        for key in datafiles.keys():
            datafiles[key] = make_unique_dict(datafiles[key])
        return datafiles

# # Read datafile to dataframe and name columns based on filename

def df_read_datafile(datafile, set_index=False):
    filename = os.path.basename(datafile)
    names = re.split(",", re.findall("\((.+)\)", filename)[0])
    df = pd.read_csv(datafile, names=names)
    if set_index:
        df = df.set_index(names[0])
    return df

# # Read datafile to dataframe and name columns based on filename (works for single file, list and dict of files)

def df_read_datafiles(datafiles):
    if isinstance(datafiles, str):
        return df_read_datafile(datafiles)
    elif isinstance(datafiles, list):
        return [df_read_datafile(datafile) for datafile in datafiles]
    elif isinstance(datafiles, dict):
        for key in datafiles.keys():
            datafiles[key] = df_read_datafiles(datafiles[key])
        return datafiles

def df_read_datadir_unique(data_dir, reqs_dict={}, keys_dict={}):
    datafiles = get_datafiles_list(data_dir)
    datafiles = filter_datafiles_list(datafiles, reqs_dict)
    datafiles = datafiles_dict(datafiles, keys_dict)
    datafiles = make_unique_dict(datafiles)
    return df_read_datafiles(datafiles)