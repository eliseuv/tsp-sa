#include "tsp_sa.hpp"

MAIN_FUNCTION
{
    // Parameters
    size_t node_count = 2048;
    Real rho = 0.8;
    Real initial_beta = 1e0;
    Real final_beta = 1e7;
    size_t total_samples = std::pow(2, 24);
    size_t n_steps = std::pow(2, 10);
    size_t n_runs = 60;

    // PRNG
    MT19937 gen(randomSeed());

    // Parse arguments
    PARSE_ARGS(node_count, rho, initial_beta, final_beta, total_samples, n_steps, n_runs);
    io::csvStream(std::cout,    STR_VAR(node_count),
                                STR_VAR(initial_beta),
                                STR_VAR(final_beta),
                                STR_VAR(total_samples),
                                STR_VAR(n_steps),
                                STR_VAR(n_runs));

    size_t n_samples = static_cast<size_t>(static_cast<double>(total_samples)/static_cast<double>(n_steps));
    Real alpha = std::pow(final_beta/initial_beta, 1.0/(n_steps-1));
    Real actual_final_beta = std::pow(alpha, n_steps-1)*initial_beta;

    // Output file
    std::string output_filename = io::filename({"data/domain2D,type=correlated,moves=swap",
                                                STR_VAR(node_count),
                                                STR_VAR(initial_beta),
                                                STR_VAR(final_beta),
                                                STR_VAR(total_samples),
                                                STR_VAR(n_steps),
                                                STR_VAR(n_runs),
                                                "(rho,final_cost_avg,final_cost_var,actual_final_beta).csv"});
    std::cout << STR_VAR(output_filename) << std::endl;

    std::vector<Real> final_costs(n_runs);

    for (size_t n = 0; n < n_runs; n++) {

        std::cout << "run " << n << std::endl;

        // Domain
        topo::EuclideanSpace2D domain(node_count);
        domain.distributeCorrelatedCoords(gen, rho);

        // Hamiltonian Cycle
        graph::KWGraph kwgraph(domain.metricMatrix());
        graph::HCycle hcycle(kwgraph);
        graph::HCycle::SimpleSwap mcmc(hcycle);

        // Annealer
        Annealer sampler(initial_beta, Annealer::geometricCooling(alpha));
        sampler.annealFixedSteps(mcmc, n_samples, n_steps);

        final_costs[n] = hcycle.cost();
        actual_final_beta = sampler.beta();

        auto [final_cost_avg, final_cost_var] = stats::vectorAverageVariance(final_costs, n);
        io::csvStream(std::cout, rho, final_cost_avg, final_cost_var, actual_final_beta);

    }

    auto [final_cost_avg, final_cost_var] = stats::vectorAverageVariance(final_costs);

    // Save data
    std::ofstream output_file(output_filename, std::ios::app);
    output_file << io::floatingPrecision<Real>;
    io::csvStream(output_file, rho, final_cost_avg, final_cost_var, actual_final_beta);
    output_file.close();

}