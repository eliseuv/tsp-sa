#! /bin/python

import numpy as np

# node_count = 2**11
# rho = 0.9
initial_beta = 1e0
final_beta = 1e7
total_samples = 2**24
n_steps = 2**14
n_runs = 120

for nc in range(4, 11+1):
    node_count = 2**nc
    for rho in np.linspace(0,1,101):
    # for r in range(1,10):
        # rho = 0.99 + 0.001*r
        print('{},{},{},{},{},{},{}'.format(node_count, rho, initial_beta, final_beta, total_samples, n_steps, n_runs))