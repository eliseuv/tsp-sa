#include "tsp_sa.hpp"

MAIN_FUNCTION
{
    // Parameters
    size_t node_count = 2048;
    Real beta = 5e4;
    size_t n_samples = std::pow(2, 26);

    // PRNG
    MT19937 gen(randomSeed());

    // Parse arguments
    PARSE_ARGS(node_count, beta, n_samples)
    io::csvStream(std::cout,    STR_VAR(node_count),
                                STR_VAR(beta),
                                STR_VAR(n_samples));

    // Output file
    std::string output_filename = io::filename({"data/domain2D,type=uniform,moves=twist",
                                                STR_VAR(node_count),
                                                STR_VAR(beta),
                                                // STR_VAR(n_samples),
                                                "(cost).csv"});
    std::cout << STR_VAR(output_filename) << std::endl;


    // Domain
    topo::EuclideanSpace2D domain(node_count);
    domain.distributeUniformly(gen);

    // Hamiltonian Cycle
    graph::KWGraph kwgraph(domain.metricMatrix());
    graph::HCycle hcycle(kwgraph);
    graph::HCycle::Twist mcmc(hcycle);

    Metropolis metropolis(beta);
    metropolis.sample(mcmc, n_samples);

    // Save data
    std::ofstream output_file(output_filename, std::ios::app);
    output_file << io::floatingPrecision<Real>;

    metropolis.sample(mcmc, n_samples, output_file);

    output_file.close();

}