#include "tsp_sa.hpp"

MAIN_FUNCTION
{
    // Parameters
    size_t node_count = 2048;
    std::vector<Real> betas = logspace(-2, 7, 100, 10);
    size_t n_samples = std::pow(2, 22);
    int run_id = 0;
    Real gamma = 5.0;
    Real x_0 = 1e-1;

    // PRNG
    MT19937 gen(randomSeed());

    // Parse arguments
    PARSE_ARGS(node_count, n_samples, gamma, x_0, run_id);
    io::csvStream(std::cout,    STR_VAR(node_count),
                                STR_VAR(n_samples),
                                STR_VAR(gamma),
                                STR_VAR(x_0),
                                STR_VAR(run_id)
                                );

    // Output file
    std::string output_filename = io::filename({"data/domain2D,type=plaw_radial,moves=twist",
                                                STR_VAR(node_count),
                                                STR_VAR(gamma),
                                                STR_VAR(x_0),
                                                STR_VAR(n_samples),
                                                STR_VAR(run_id),
                                                "(beta,scaled_cost_avg).csv"});
    std::cout << STR_VAR(output_filename) << std::endl;

    // Domain
    topo::EuclideanSpace2D domain(node_count);

    // domain.distributeUniformly(gen);
    // domain.distributeCorrelatedCoords(gen, rho);

    PowerLawDistribution plaw_dist(gamma, x_0);
    domain.distributeRadially(gen, plaw_dist);

    // SymmetricPowerLawDistribution plaw_dist(gamma, x_0);
    // domain.setAllCoordsFromDist(plaw_dist, gen);

    // Gnuplot gplt;
    // domain.plot(gplt);
    // return 0;

    Real scaling = domain.scaling();

    // Hamiltonian Cycle
    graph::KWGraph kwgraph(domain.metricMatrix());
    graph::HCycle hcycle(kwgraph);
    graph::HCycle::Twist mcmc(hcycle);

    for (Real beta : betas) {
        Metropolis metropolis(beta);
        metropolis.sample(mcmc, n_samples);
        auto [cost_avg, cost_var] = metropolis.energyStats(mcmc, n_samples);
        io::csvStream(std::cout, beta, scaling*cost_avg);

        // Save data
        std::ofstream output_file(output_filename, std::ios::app);
        output_file << io::floatingPrecision<Real>;
        io::csvStream(output_file, beta, scaling*cost_avg);
        output_file.close();

    }

}