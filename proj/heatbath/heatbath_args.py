#! /bin/python

import numpy as np

node_count = 2**11
n_samples = 2**22
# rho_vals = sorted(np.append(np.linspace(0, 1, 101), (0.99+0.001*np.arange(1,10))))
gamma_vals = [1.5 + k*0.25 for k in range(15)]
x_0_vals = np.logspace(-2, 0, 3, base=10)

for run_id in range(0):
    for x_0 in x_0_vals:
        for gamma in gamma_vals:
            print('{},{},{},{},{}'.format(node_count, n_samples, gamma, x_0, run_id))