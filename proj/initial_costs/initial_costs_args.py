#! /bin/python

import numpy as np

# node_count = 2**11
n_samples = 2**14
# p_vals = np.logspace(0, 6, num=100, base=10)
# rho_vals = (p_vals - 1)/p_vals
# p_vals = np.logspace(0, -8, num=17, base=10)
# rho_vals = 1 - p_vals
# gamma_vals = [1.0 + 0.1*k for k in range(1, 60+1)]
# x_0_vals = [10.0**k for k in range(-4, 2+1)]
rho = 1.0

for nc in range(4,11+1):
    node_count = 2**nc
    # for rho in rho_vals:
    print('{},{},{}'.format(node_count, n_samples, rho))
    pass