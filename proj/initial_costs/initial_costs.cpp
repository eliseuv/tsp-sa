#include "tsp_sa.hpp"

MAIN_FUNCTION
{
    // Parameters
    size_t node_count = 2048;
    size_t n_samples = std::pow(2, 10);
    Real rho = 0.9;
    // Real gamma = 5.0;
    // Real x_0 = 1e-1;

    // PRNG
    MT19937 gen(randomSeed());

    // Parse arguments
    PARSE_ARGS(node_count, n_samples, rho);
    io::csvStream(std::cout,    STR_VAR(node_count),
                                STR_VAR(n_samples),
                                STR_VAR(rho));

    // Output file
    std::string output_filename = io::filename({"data/domain2D,type=correlated",
                                                STR_VAR(node_count),
                                                // STR_VAR(x_0),
                                                STR_VAR(n_samples),
                                                "(rho,initial_cost_avg,initial_cost_var).csv"});
    std::cout << STR_VAR(output_filename) << std::endl;

    std::vector<Real> initial_costs(n_samples);

    for (size_t n = 0; n < n_samples; n++) {

        // Domain
        topo::EuclideanSpace2D domain(node_count);

        UniformRealDistribution dist(-0.5, 0.5);
        domain.distributeCorrelatedCoords(gen, dist, rho);

        // PowerLawDistribution plaw_dist(gamma, x_0);
        // domain.distributeRadially(gen, plaw_dist);

        // SymmetricPowerLawDistribution plaw_dist(gamma, x_0);
        // domain.setAllCoordsFromDist(plaw_dist, gen);

        // Gnuplot gplt;
        // domain.plot(gplt);
        // return 0;

        initial_costs[n] = graph::defaultHCycleCost(domain);

        auto [initial_cost_avg, initial_cost_var] = stats::vectorAverageVariance(initial_costs, n+1);
        io::csvStream(std::cout, n, rho, initial_cost_avg, initial_cost_var);

    }

    auto [initial_cost_avg, initial_cost_var] = stats::vectorAverageVariance(initial_costs);

    // Save data
    std::ofstream output_file(output_filename, std::ios::app);
    output_file << io::floatingPrecision<Real>;
    io::csvStream(output_file, rho, initial_cost_avg, initial_cost_var);
    output_file.close();

}