import numba

import numpy as np

def avg_dist_uniform_2d(point_count):
    return ((point_count/15)*(2+np.sqrt(2)+5*np.log(np.sqrt(2)+1)))

def plaw_pdf(x, gamma, x0):
    return np.where(np.abs(x) > x0, (gamma-1)/(2*np.power(x0, 1-gamma))*np.power(np.abs(x), -gamma), np.nan)

def draw_from_plaw(N, gamma, x0):
    sign = 2*np.random.binomial(1, 0.5, size=N)-1
    plaw = x0*np.power(1-np.random.rand(N), 1.0/(1-gamma))
    return sign*plaw