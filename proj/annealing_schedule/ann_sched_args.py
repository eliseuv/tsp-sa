#! /bin/python

import numpy as np

node_count = 2**11
initial_beta = 1e0
final_beta = 1e7
# total_samples = 2**24
# n_steps = 2**14
n_runs = 60
rho_vals = [0.9, 1.0]
# rho_vals = np.delete(1 - 1.0/np.logspace(1, 9, 101), 0)
# one_minus_rho_vals = 1.0/np.logspace(0, 1, 25)
# print(one_minus_rho_vals)

for ts in range(13, 28+1):
    total_samples = 2**ts
    for ns in range(1, ts+1):
    # ns = ts
        n_steps = 2**ns
        for rho in rho_vals:
            print('{},{},{},{},{},{},{}'.format(node_count, initial_beta, final_beta, total_samples, n_steps, n_runs, rho))
            pass