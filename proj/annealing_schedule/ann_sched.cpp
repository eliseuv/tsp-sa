#include "tsp_sa.hpp"

MAIN_FUNCTION
{
    // Parameters
    size_t node_count = 2048;
    Real initial_beta = 1e0;
    Real final_beta = 1e7;
    size_t total_samples = std::pow(2, 24);
    size_t n_steps = 1024;
    size_t n_runs = 60;
    Real rho = 1.0;
    // Real one_minus_rho = 0;

    // PRNG
    MT19937 gen(randomSeed());

    // Parse arguments
    PARSE_ARGS(node_count, initial_beta, final_beta, total_samples, n_steps, n_runs, rho)
    io::csvStream(std::cout,    STR_VAR(node_count),
                                STR_VAR(initial_beta),
                                STR_VAR(final_beta),
                                STR_VAR(total_samples),
                                STR_VAR(n_steps),
                                STR_VAR(n_runs),
                                STR_VAR(rho));

    size_t n_samples = static_cast<size_t>(static_cast<double>(total_samples)/static_cast<double>(n_steps));
    Real alpha = std::pow(final_beta/initial_beta, 1.0/(n_steps-1));
    Real actual_final_beta;// = std::pow(alpha, n_steps-1)*initial_beta;
    // Output file
    std::string output_filename = io::filename({"data/domain2D,type=correlated,moves=twist",
                                                STR_VAR(node_count),
                                                STR_VAR(rho),
                                                STR_VAR(initial_beta),
                                                STR_VAR(final_beta),
                                                // STR_VAR(total_samples),
                                                // STR_VAR(n_steps),
                                                STR_VAR(n_runs),
                                                "(total_samples,final_cost_avg,final_cost_var,n_steps,alpha,actual_final_beta).csv"});
    std::cout << STR_VAR(output_filename) << std::endl;

    // Statistics
    std::vector<Real> final_costs(n_runs, 0);

    for (size_t n = 0; n < n_runs; n++) {
        std::cout << "System " << n << std::endl;

        // Domain
        topo::EuclideanSpace2D domain(node_count);
        // domain.distributeUniformly(gen);

        // Real rho = 1 - one_minus_rho;
        UniformRealDistribution dist(-0.5, 0.5);
        domain.distributeCorrelatedCoords(gen, dist, rho);

        // Hamiltonian Cycle
        graph::KWGraph kwgraph(domain.metricMatrix());
        graph::HCycle hcycle(kwgraph);
        graph::HCycle::Twist mcmc(hcycle);

        // Annealing
        Annealer sampler(initial_beta, Annealer::geometricCooling(alpha));
        sampler.annealFixedSteps(mcmc, n_samples, n_steps);

        actual_final_beta = sampler.beta();
        final_costs[n] = hcycle.cost();

        auto [final_cost_avg, final_cost_var] = stats::vectorAverageVariance(final_costs, n+1);
        io::csvStream(std::cout, total_samples, final_cost_avg, final_cost_var, n_steps, alpha, actual_final_beta);

        // std::cin.get();

    }

    auto [final_cost_avg, final_cost_var] = stats::vectorAverageVariance(final_costs);

    // Save data
    std::ofstream output_file(output_filename, std::ios::app);
    output_file << io::floatingPrecision<Real>;
    io::csvStream(output_file, total_samples, final_cost_avg, final_cost_var, n_steps, alpha, actual_final_beta);
    output_file.close();

}