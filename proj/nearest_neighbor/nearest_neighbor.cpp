#include "tsp_sa.hpp"

MAIN_FUNCTION
{
    // Parameters
    size_t node_count = 2048;
    size_t n_samples = std::pow(2, 10);
    Real rho = 0.9;

    // PRNG
    MT19937 gen(randomSeed());

    // Parse arguments
    PARSE_ARGS(node_count, n_samples, rho)
    io::csvStream(std::cout,    STR_VAR(node_count),
                                STR_VAR(n_samples),
                                STR_VAR(rho));

    // Output file
    std::string output_filename = io::filename({"data/domain2D,type=correlated",
                                                STR_VAR(node_count),
                                                STR_VAR(n_samples),
                                                "(rho,nn_cost_avg,nn_cost_var).csv"});
    std::cout << STR_VAR(output_filename) << std::endl;

    // Costs
    std::vector<Real> nn_costs(n_samples);

    for (size_t n = 0; n < n_samples; n++) {

        // Domain
        topo::EuclideanSpace2D domain(node_count);

        // domain.distributeUniformly(gen);

        UniformRealDistribution dist(-0.5, 0.5);
        domain.distributeCorrelatedCoords(gen, dist, rho);

        // Hamiltonian Cycle
        graph::KWGraph kwgraph(domain.metricMatrix());
        graph::HCycle hcycle(kwgraph);

        auto [state, cost] = graph::nearestNeighbourCycleMin(kwgraph);

        nn_costs[n] = cost;
        auto [nn_cost_avg, nn_cost_var] = stats::vectorAverageVariance(nn_costs, n+1);

        io::csvStream(std::cout, n, rho, nn_cost_avg, nn_cost_var);

    }

    auto [nn_cost_avg, nn_cost_var] = stats::vectorAverageVariance(nn_costs);

    // Save data
    std::ofstream output_file(output_filename, std::ios::app);
    output_file << io::floatingPrecision<Real>;
    io::csvStream(output_file, rho, nn_cost_avg, nn_cost_var);
    output_file.close();

}