#! /bin/python

import numpy as np

# node_count = 2**11
n_samples = 128
rho_vals = np.concatenate([np.linspace(0, 0.9, 10), np.linspace(0.91, 1, 10)])
# print(rho_vals)

for nc in range(4, 11+1):
    node_count = 2**nc
    for rho in sorted(rho_vals):
        print('{},{},{}'.format(node_count, n_samples, rho))
        # pass