#include "tsp_sa.hpp"

int main(int argc, char *argv[]) {

  // Parameters
  // const size_t node_count = 2048;
  const size_t n_frames = 15 * 30;
  const std::vector<Real> betas(logspace(-2, 7, n_frames, 10));
  const size_t n_samples = pow(2, 30);

  // PRNG
  MT19937 gen(randomSeed());

  // Domain
  // topo::EuclideanSpace2D domain(node_count);
  // domain.distributeUniformly(gen);

  topo::SphereSurface domain(
      io::loadMatrixCSV<topo::PointSetFiniteMetricSpace::PointsMatrix>(
          "../../datasets/tsp_nodes/br5569_coords.csv"));
  topo::EuclideanSpace2D projection = domain.plateCarree();
  size_t node_count = domain.pointCount();

  // Hamiltonian Cycle
  graph::KWGraph kwgraph(domain.metricMatrix());
  graph::HCycle hcycle(kwgraph);
  graph::HCycle::Twist mcmc(hcycle);

  // Output file
  std::string output_filename =
      io::filename({"data/br5569", STR_VAR(node_count), STR_VAR(n_samples),
                    "(beta,cost,state).csv"});
  std::cout << STR_VAR(output_filename) << std::endl;

  std::ofstream output_file(output_filename, std::ios::out);

  // Gnuplot
  Gnuplot gplt;
  gplt("set terminal png size 1000,1000");
  gplt("set size ratio 1");
  gplt("unset key");
  gplt("unset tics");
  gplt("set style line 1 lc rgb '#0060ad' lt 1 lw 0.5 pt 7 pi -1 ps 0.5");
  gplt("set pointintervalbox 0.6");

  int progress = 0;

  for (size_t frame = 0; frame < n_frames; frame++) {
    Real beta = betas[frame];
    Metropolis metropolis(beta);
    metropolis.sample(mcmc, n_samples);

    std::cout << beta << " " << hcycle.cost() << std::endl;

    output_file << beta << "," << hcycle.cost();
    for (size_t k = 0; k < hcycle.deg(); k++) {
      output_file << "," << hcycle.state(k);
    }
    output_file << std::endl;

    /*

    // New file
    gplt(concatString("set output \"fig/", frame, ".png\""));

    // Annotations
    gplt("unset label");
    gplt(concatString("set label \"T = ", (1.0 / beta),
                      "\" at -72,-32 left font \",18\" front"));
    gplt(concatString("set label \"Cost = ", hcycle.cost(),
                      "\" at -33,-32 right font \",18\" front"));

    // Plot
    gplt.beginPoints(
        2, {"with lines linecolor rgb \"blue\"",
            "with points pointtype 7 pointsize 0.7 linecolor rgb \"red\""});
    for (size_t n = 0; n < projection.pointCount(); n++) {
      gplt.point2D(projection.coord(n, 0), projection.coord(n, 1));
    }
    gplt.endPoints();
    for (size_t n = 0; n < projection.pointCount(); n++) {
      gplt.point2D(projection.coord(hcycle.state(n), 0),
                   projection.coord(hcycle.state(n), 1));
    }
    gplt.endPoints();

    // hcycle.plot(projection, gplt, "with lines");
    // projection.plot(gplt, "with points pointtype 7 pointsize 0.7 linecolor
    // rgb \"red\"");

    */

    // Progress
    Real new_progress = 100 * static_cast<Real>(frame) / n_frames;
    if (std::floor(new_progress) != progress) {
      progress = std::floor(new_progress);
      std::cout << progress << "%" << std::endl;
    }
  }

  output_file.close();

  return 0;
}
