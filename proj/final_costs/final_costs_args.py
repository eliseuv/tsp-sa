#! /bin/python

import numpy as np

# node_count = 2**11
initial_beta = 1e0
final_beta = 1e7
total_samples = 2**28
n_steps = total_samples
n_runs = 120
# rho_vals = sorted(np.append(np.linspace(0, 1, 101), (0.99+0.001*np.arange(1,10))))
# p_vals = np.logspace(0, 10, num=11, base=10)
# rho_vals = 1 - p_vals
# print(rho_vals)
rho = 1.0
# gamma_vals = [1.5 + k*0.25 for k in range(15)]
# print(exponents)

# for gamma in gamma_vals:
#     x_0 = 1e-2
#     print('{},{},{},{},{},{},{},{}'.format(node_count, initial_beta, final_beta, total_samples, n_steps, gamma, x_0, n_runs))

for nc in range(4, 11+1):
    node_count = 2**nc
    # for e in exponents:
    print('{},{},{},{},{},{},{}'.format(node_count, initial_beta, final_beta, total_samples, n_steps, rho, n_runs))
    pass