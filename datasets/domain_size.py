import sys
import re
import pandas as pd
import numpy as np

data_filenames = sys.argv[1:]
print(data_filenames)

for data_filename in data_filenames:
    #print(data_filename)
    # Read file and calulate average
    tsp_nodes = pd.read_csv(data_filename, sep=' ', header=None)
    size_x = tsp_nodes[0].max() - tsp_nodes[0].min()
    size_y = tsp_nodes[1].max() - tsp_nodes[1].min()
    print(data_filename, np.sqrt(size_x*size_y))