#!/bin/zsh

# Get data file from argument
data_file=$1
header_size=$2

# Convert file
dos2unix $data_file
# Remove header                     | Remove EOF str | Remove index col              > path/file.dat
tail $data_file -n +$header_size    | sed '/EOF/d'   | awk -F' ' '{print $3, $2}'    > ${file%.*}.dat

#rm $data_file