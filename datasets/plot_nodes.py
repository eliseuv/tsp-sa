import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

print("args = ", sys.argv)

data_file = sys.argv[1]

# Read data
data = pd.read_csv(data_file, header=None, sep=" ")
print(data)

# Plot
ax = data.plot.scatter(x=0, y=1, s=1, title=data_file, legend=False, grid=False)
ax.set_aspect('equal')
plt.show()
