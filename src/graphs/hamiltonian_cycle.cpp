#include "graphs/hamiltonian_cycle.hpp"

namespace graph {

// # Nearest Neighbour Cycle

std::pair<Permutation::State, Real>
nearestNeighbourCycle(const topo::FiniteMetricSpace<Real> &metric_space,
                      size_t initial_node) {
  Permutation::State state =
      Permutation::defaultState(metric_space.elementCount());
  std::swap(state[0], state[initial_node]);

  Real cost = 0;

  for (Permutation::State::iterator it_ref = state.begin();
       it_ref != (state.end() - 1); ++it_ref) {
    Permutation::State::iterator it_nn = it_ref + 1;
    Real cost_nn = metric_space.metric(*it_ref, *it_nn);
    for (Permutation::State::iterator it_test = it_ref + 2;
         it_test != state.end(); ++it_test) {
      Real cost_test = metric_space.metric(*it_ref, *it_test);
      if (cost_test < cost_nn) {
        it_nn = it_test;
        cost_nn = cost_test;
      }
    }
    std::iter_swap(it_ref + 1, it_nn);
    cost += cost_nn;
  }
  cost += metric_space.metric(state.back(), state.front());

  return std::make_pair(state, cost);
}

std::pair<Permutation::State, Real>
nearestNeighbourCycle(const graph::KWGraph &kwgraph, size_t initial_node) {
  Permutation::State state = Permutation::defaultState(kwgraph.nodeCount());
  std::swap(state[0], state[initial_node]);

  Real cost = 0;

  for (Permutation::State::iterator it_ref = state.begin();
       it_ref != (state.end() - 1); ++it_ref) {
    Permutation::State::iterator it_nn = it_ref + 1;
    Real cost_nn = kwgraph.weight(*it_ref, *it_nn);
    for (Permutation::State::iterator it_test = it_ref + 2;
         it_test != state.end(); ++it_test) {
      Real cost_test = kwgraph.weight(*it_ref, *it_test);
      if (cost_test < cost_nn) {
        it_nn = it_test;
        cost_nn = cost_test;
      }
    }
    std::iter_swap(it_ref + 1, it_nn);
    cost += cost_nn;
  }
  cost += kwgraph.weight(state.back(), state.front());

  // std::cout << initial_node << "\t" << cost << std::endl;

  return std::make_pair(state, cost);
}

std::pair<Permutation::State, Real>
nearestNeighbourCycleMin(const graph::KWGraph &kwgraph) {
  const size_t N = kwgraph.nodeCount();
  Real cost_min = std::numeric_limits<Real>::max();
  Permutation::State state_min(N);
  for (size_t i = 0; i < N; i++) {
    auto [state, cost] = nearestNeighbourCycle(kwgraph, i);
    if (cost < cost_min) {
      cost_min = cost;
      state_min = state;
    }
  }
  return std::make_pair(state_min, cost_min);
}

// # Hamiltonian Cycle

void HCycle::nearest_neighbour(size_t initial_node) {
  for (size_t n = 0; n < this->deg(); n++)
    this->state_[n] = n;

  std::swap(this->state_.front(), this->state_[initial_node]);

  cost_ = 0;

  for (Permutation::State::iterator it_ref = this->state_.begin();
       it_ref != (this->state_.end() - 1); ++it_ref) {
    Permutation::State::iterator it_nn = it_ref + 1;
    Real cost_nn = this->kwgraph_->weight(*it_ref, *it_nn);
    for (Permutation::State::iterator it_test = it_ref + 2;
         it_test != this->state_.end(); ++it_test) {
      Real cost_test = this->kwgraph_->weight(*it_ref, *it_test);
      if (cost_test < cost_nn) {
        it_nn = it_test;
        cost_nn = cost_test;
      }
    }
    std::iter_swap(it_ref + 1, it_nn);
    cost_ += cost_nn;
  }
  cost_ += this->kwgraph_->weight(this->state_.back(), this->state_.front());
}

void HCycle::calculateCost() {
  const size_t N = this->deg();
  cost_ = kwgraph_->weight(this->state(N - 1), this->state(0));
  for (size_t i = 0; i < N - 1; i++)
    cost_ += kwgraph_->weight(this->state(i), this->state(i + 1));
}

void HCycle::print(std::ostream &output_stream) const {
  output_stream << "cost = " << cost() << std::endl;
  this->printState(output_stream);
}

void HCycle::plot(const topo::EuclideanSpace &space, Gnuplot &gplt,
                  const std::string &opts) const {
  if (this->deg() != space.pointCount()) {
    std::cerr
        << "ERROR: Point count must be the same as Hamiltonian Cycle degree!"
        << std::endl;
    return;
  }
  if (space.dim() == 2) {
    gplt.beginPoints(opts);
    for (size_t u = 0; u < this->deg(); u++)
      gplt.point2D(space.coord(this->state(u), 0),
                   space.coord(this->state(u), 1));
    gplt.point2D(space.coord(this->state(0), 0),
                 space.coord(this->state(0), 1));
    gplt.endPoints();
  }
}

} // namespace graph
