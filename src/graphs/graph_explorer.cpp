#include "tools/file_io.hpp"

#include "graphs/graph_explorer.hpp"

namespace graph {

// # Dynamic Graph Explorer

// - Constructor

GraphExplorer::GraphExplorer(const KWGraph& kwgraph, size_t initial_node)
: kwgraph_(&kwgraph),
  dist_(0, kwgraph.nodeCount()-2),
  state_(1, initial_node), inv_state_(kwgraph.nodeCount()),
  cost_(0),
  closed_cycle_(false)
{
    state_.reserve(kwgraph.nodeCount());
    for (size_t n = 0; n < kwgraph.nodeCount(); n++)
        inv_state_[n] = n;
    inv_state_.erase(inv_state_.begin()+initial_node);
    next_node_ = inv_state_.begin();
}

// - Markov Chain

void GraphExplorer::setCurrentState(const State& state) {
    state_ = state;
    state_.reserve(kwgraph_->nodeCount());
    inv_state_.clear();
    inv_state_.reserve(kwgraph_->nodeCount());
    for (size_t n = 0; n < kwgraph_->nodeCount(); n++) {
        auto pos = std::find(state_.begin(), state_.end(), n);
        if (pos == state_.end()) {
            inv_state_.emplace_back(n);
        }
    }
    calculateCurrentValue();
}

// - MCMC

void GraphExplorer::calculateCurrentValue() {   
    cost_ = 0;
    for (size_t i = 0; i < state_.size()-1; i++)
        cost_ += kwgraph_->weight(state_[i], state_[i+1]);
    if (closed_cycle_) cost_ += kwgraph_->weight(state_.back(), state_.front());
}

// - Determinisitic selection

void GraphExplorer::selectNearestNeighbour() {
    const size_t current_node = getCurrentNode();
    State::iterator nearest_neighbour = inv_state_.begin();
    Real nearest_neighbour_cost = kwgraph_->weight(current_node, *nearest_neighbour);
    for (State::iterator it = (inv_state_.begin()+1); it != inv_state_.end(); it++) {
        Real cost_test = kwgraph_->weight(current_node, *it);
        if (cost_test < nearest_neighbour_cost) {
            nearest_neighbour = it;
            nearest_neighbour_cost = cost_test;
        }
    }
    next_node_ = nearest_neighbour;
}

// - Close cycle

bool GraphExplorer::closeCycle() {
    if ((!closed_cycle_) && (persistence() == 0)) {
        cost_ += kwgraph_->weight(state_.back(), state_.front());
        closed_cycle_ = true;
    }
    return closed_cycle_;
}

// - Print

void GraphExplorer::print(std::ostream& output_stream) const {
    io::csvStream(output_stream, cost_, persistence());
    // io::printVector(state_);
}

void GraphExplorer::printDebug(std::ostream& output_stream) const {
    output_stream << "\nSTATE =" << std::endl;
    for (auto n : state_) output_stream << n << ',';
    output_stream << std::endl;
    output_stream << "INV STATE =" << std::endl;
    for (auto n : inv_state_) output_stream << n << ',';
    output_stream << std::endl;
    output_stream << "CURRENT NODE = " << getCurrentNode() << std::endl;
    output_stream << "NEXT NODE = " << *next_node_ << std::endl;
    output_stream << "COST = " << cost_ << std::endl;
}

} // graph