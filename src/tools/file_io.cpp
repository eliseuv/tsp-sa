#include "tools/file_io.hpp"

namespace io
{

    // Count number of lines and rows in a file
    std::pair<size_t, size_t> getMatrixShape(const std::string& filename,
                                          const std::string& delim)
    {
        size_t rows = 0, cols = 0;
        // Open data file
        std::ifstream data_file;
        if (!data_file.is_open()) { // File not opened yet
        data_file.open(filename, std::ios::in);
        if(!data_file)
            std::cerr << "Failed to open " << filename << std::endl;
        else { // File opened
            std::string line;
            std::getline(data_file, line);
            // Parse columns
            cols = 1;
            for (std::string::const_iterator i = line.begin(); i != line.end(); ++i)
                // if ((is delim) and (ignore repeated delims) and (not the last delim in the line))
                if ((delim.find(*i) != std::string::npos) && (*(i-1) != *i) && (i + 1 != line.end())) cols++;
            // Parse rows
            rows = 1;            
            while(std::getline(data_file, line))
                rows++;
            } // File opened
            data_file.close();
        } // File not openend

        return std::pair<size_t, size_t>{rows, cols};
    }
}
