#include "maths/statistics.hpp"

namespace stats {

    // # Histogram

    // - Add values
    void Histogram::operator()(const Real& new_value) {
        
        // Find bin
        auto it = std::upper_bound(bins_.begin(), bins_.end(), new_value);

        // Ignore if outside range
        if ((it == bins_.begin()) || (it == bins_.end())) return;

        // Add value
        auto bin = std::distance(bins_.begin(), it) - 1;
        hist_[bin]++;
        this->count_++;
    }

} // stats