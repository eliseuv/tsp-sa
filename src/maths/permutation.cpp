#include "maths/permutation.hpp"

// # Dynamic Permutation

// - State

bool Permutation::check(const State& state) {
    for (size_t i = 0; i < state.size(); i++) {
        bool found = false;
        for (size_t s : state) {
        if (s == i) {
            found = true;
            break;
        }
        }
        if (!found) return false;
    }
    return true;
}

// - Equality

bool Permutation::operator==(const Permutation& other) const {
    if (this->deg() != other.deg()) return false;

    for (size_t i = 0; i < this->deg(); i++)
        if(this->state(i) != other.state(i)) return false;
    
    return true;  
}

// - Composition: G x G -> G

Permutation Permutation::operator*(const Permutation& other) const {
    ASSERT(this->deg() == other.deg())

    size_t N = deg();
    
    State state(N);
    for (size_t i = 0; i < N; i++)
        state[i] = this->state(other.state(i));

    return Permutation(std::move(state));
}

// - Print

void Permutation::printState(std::ostream& output_stream) const {
    for (auto i : state_)
        output_stream << i << ',';
    output_stream << std::endl;
}
