#include "maths/topology.hpp"

namespace topo {

// # Point Set Finite Metric Space

void PointSetFiniteMetricSpace::setCoordFromDist(size_t i,
                                                 RealDistribution &dist,
                                                 MT19937 &gen) {
  for (size_t u = 0; u < pointCount(); u++)
    point(u)(i) = dist(gen);
}

void PointSetFiniteMetricSpace::setAllCoordsFromDist(RealDistribution &dist,
                                                     MT19937 &gen) {
  for (size_t i = 0; i < dim(); i++)
    setCoordFromDist(i, dist, gen);
}

void PointSetFiniteMetricSpace::print(std::ostream &output_stream) const {
  for (size_t u = 0; u < pointCount(); u++) {
    for (size_t i = 0; i < dim(); i++) {
      output_stream << points_(i, u);
      if (i != (dim() - 1))
        output_stream << ",";
    }
    output_stream << std::endl;
  }
}

void EuclideanSpace::distributeUniformly(MT19937 &gen) {
  UniformRealDistribution u_dist(0, 1);
  for (size_t u = 0; u < this->pointCount(); u++) {
    for (size_t i = 0; i < this->dim(); i++) {
      this->coord(u, i) = u_dist(gen);
    }
  }
}

void EuclideanSpace::distributeUniformly(MT19937 &gen,
                                         std::initializer_list<Real> size_il) {
  ASSERT(size.size() == this->dim())
  std::vector<Real> size_vec(size_il);
  UniformRealDistribution u_dist(0, 1);
  for (size_t u = 0; u < this->pointCount(); u++) {
    for (size_t i = 0; i < this->dim(); i++) {
      this->coord(u, i) = size_vec[i] * u_dist(gen);
    }
  }
}

void EuclideanSpace::distributeRadially(MT19937 &gen,
                                        RealDistribution &radial_dist) {
  NormalDistribution gauss_dist(1, 0);
  for (size_t u = 0; u < this->pointCount(); u++) {
    for (size_t i = 0; i < this->dim(); i++)
      this->coord(u, i) = gauss_dist(gen);
    this->point(u) *= radial_dist(gen) / this->point(u).norm();
  }
}

void PointSetFiniteMetricSpace::plot(Gnuplot &gplt,
                                     const std::string &opts) const {
  if (dim() == 2) {
    gplt.beginPoints(opts);
    for (size_t u = 0; u < pointCount(); u++) {
      gplt.point2D(points_(0, u), points_(1, u));
    }
    gplt.endPoints();
  }
}

// # Euclidean Domain

void EuclideanSpace::applyScale(Real scale) {
  this->points_ = scale * this->points_;
}

// # Euclidean Domain 2D

void EuclideanSpace2D::distributeUniformly(MT19937 &gen, Real size_x,
                                           Real size_y) {
  UniformRealDistribution u_dist(0, 1);
  for (size_t u = 0; u < this->pointCount(); u++) {
    this->point(u)[0] = size_x * u_dist(gen);
    this->point(u)[1] = size_y * u_dist(gen);
  }
}

void EuclideanSpace2D::distributeRadially(MT19937 &gen,
                                          RealDistribution &radial_dist) {
  UniformRealDistribution angle_dist(0, 2 * PI);
  for (size_t u = 0; u < this->pointCount(); u++) {
    Real theta = angle_dist(gen);
    Real radius = radial_dist(gen);
    this->point(u)[0] = radius * std::cos(theta);
    this->point(u)[1] = radius * std::sin(theta);
  }
}

void EuclideanSpace2D::distributeCorrelatedCoords(MT19937 &gen,
                                                  RealDistribution &dist,
                                                  Real rho) {
  CorrelatedPairSource corr_pair(rho);
  for (size_t u = 0; u < this->pointCount(); u++) {
    auto [x, y] = corr_pair(gen, dist);
    this->point(u)[0] = x;
    this->point(u)[1] = y;
  }
}

Eigen::Matrix2d EuclideanSpace2D::domainBounds() const {
  Eigen::Matrix2d bounds_matrix = Eigen::Matrix2d::Zero();
  bounds_matrix.col(0) = this->points_.rowwise().minCoeff();
  bounds_matrix.col(1) = this->points_.rowwise().maxCoeff();
  return bounds_matrix;
}

Real EuclideanSpace2D::scaling() const {
  Eigen::Matrix2d bounds_matrix = domainBounds();
  Eigen::Vector2d domain_size = bounds_matrix.col(1) - bounds_matrix.col(0);
  return (1.0 / std::sqrt(domain_size(0) * domain_size(1)));
}

// # Cylinder Strip

EuclideanSpace2D CylinderStrip::unfold() const {
  EuclideanSpace2D unfolded(this->pointCount());

  for (size_t u = 0; u < this->pointCount(); u++)
    unfolded.point(u) = this->point(u);

  return unfolded;
}

// # Sphere Surface

void SphereSurface::distributeUniformly(MT19937 &gen) {

  UniformDistribution<Real> z_dist(-1.0, 1.0);
  UniformDistribution<Real> azimuthal_dist(0.0, 2 * PI);

  // Generate nodes in random positions
  for (size_t u = 0; u < this->elementCount(); u++) {
    Real azimuthal = azimuthal_dist(gen);
    Real polar = std::acos(z_dist(gen));
    this->coord(u, 0) = azimuthal;
    this->coord(u, 1) = polar;
  }
}

void SphereSurface::distributeUniformly(MT19937 &gen, Real theta) {

  Real z_min = std::cos(theta);
  UniformDistribution<Real> z_dist(z_min, 1.0);
  UniformDistribution<Real> azimuthal_dist(0.0, 2 * PI);

  // Generate nodes in random positions
  for (size_t u = 0; u < this->elementCount(); u++) {
    Real azimuthal = azimuthal_dist(gen);
    Real polar = std::acos(z_dist(gen));
    this->coord(u, 0) = azimuthal;
    this->coord(u, 1) = polar;
  }
}

EuclideanSpace2D SphereSurface::plateCarree() const {
  EuclideanSpace2D plate_carree(this->pointCount());

  for (size_t u = 0; u < this->pointCount(); u++)
    plate_carree.point(u) = this->point(u);

  return plate_carree;
}

EuclideanSpace2D SphereSurface::mercator() const {
  EuclideanSpace2D mercator(this->pointCount());

  for (size_t u = 0; u < this->pointCount(); u++) {
    Real lon_rad = this->coord(u, 0) - PI;
    Real lat_rad = -this->coord(u, 1) + PI_2;
    mercator.coord(u, 0) = lon_rad * DEG_RAD;
    mercator.coord(u, 1) = std::log(std::tan(PI_4 + 0.5 * lat_rad)) * DEG_RAD;
  }

  return mercator;
}

} // namespace topo
